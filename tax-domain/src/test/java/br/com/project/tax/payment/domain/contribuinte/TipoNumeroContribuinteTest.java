package br.com.project.tax.payment.domain.contribuinte;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TipoNumeroContribuinteTest {

    @Test
    void deveRetornarTipoCNPJ() {

        Assertions
                .assertEquals(TipoNumeroContribuinte.CNPJ, TipoNumeroContribuinte.of(1));

    }

    @Test
    void deveRetornarTipoCPF() {

        Assertions
                .assertEquals(TipoNumeroContribuinte.CPF, TipoNumeroContribuinte.of(2));

    }

    @Test
    void deveRetornarTipoOutro() {

        Assertions
                .assertEquals(TipoNumeroContribuinte.NAO_IDENTIFICADO, TipoNumeroContribuinte.of(99));

    }

}