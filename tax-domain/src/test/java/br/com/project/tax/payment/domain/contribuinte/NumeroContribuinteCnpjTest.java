package br.com.project.tax.payment.domain.contribuinte;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

class NumeroContribuinteCnpjTest {

    private final Predicate<String> predicado = TipoNumeroContribuinte.CNPJ.predicado();

    @Test
    void cenarioCadeiaComPrimeiroDigitoVerificadorErrado() {

        Assertions.assertFalse(predicado.test("11.222.333/0001-71"));

    }

    @Test
    void cenarioCadeiaComSegundoDigitoVerificadorErrado() {

        Assertions.assertFalse(predicado.test("11.222.333/0001-82"));

    }

    @Test
    void cenarioCNPJValidos() {

        Assertions.assertTrue(predicado.test("11.222.333/0001-81"));
        Assertions.assertTrue(predicado.test("97591611000193"));
        Assertions.assertTrue(predicado.test("02139724000146"));

    }

    @Test
    void cenarioCNPJsInvalidos() {

        Assertions.assertFalse(predicado.test(null));
        Assertions.assertFalse(predicado.test(""));
        Assertions.assertFalse(predicado.test("021397240001466"));
        Assertions.assertFalse(predicado.test("00000000000000"));
        Assertions.assertFalse(predicado.test("11111111111111"));
        Assertions.assertFalse(predicado.test("22222222222222"));
        Assertions.assertFalse(predicado.test("33333333333333"));
        Assertions.assertFalse(predicado.test("44444444444444"));
        Assertions.assertFalse(predicado.test("55555555555555"));
        Assertions.assertFalse(predicado.test("66666666666666"));
        Assertions.assertFalse(predicado.test("77777777777777"));
        Assertions.assertFalse(predicado.test("88.888.888/8888-88"));
        Assertions.assertFalse(predicado.test("99999999999999"));

    }

}
