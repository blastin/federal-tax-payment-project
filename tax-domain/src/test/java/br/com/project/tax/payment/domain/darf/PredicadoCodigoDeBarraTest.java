package br.com.project.tax.payment.domain.darf;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PredicadoCodigoDeBarraTest {

    private final PredicadoCodigoDeBarra predicado = new PredicadoCodigoDeBarra();

    @Test
    void codigoDeBarraValido() {
        final String codigoDeBarra = DocumentoArrecadacao.mock().getCodigoDeBarra();
        Assertions.assertTrue(predicado.test(codigoDeBarra));
    }

    @Test
    void codigoDeBarraInvalido() {
        final String codigoDeBarra = "85870000004734504321701907161701904720320216";
        Assertions.assertFalse(predicado.test(codigoDeBarra));
    }

    @Test
    void cenarioCodigoBarraComPrimeiroDigitoVerificadorIncorreto() {
        Assertions.assertFalse(predicado.test("85800000004734504321701907161701904720320112"));
    }

    @Test
    void cenarioCodigoBarraComSegundoDigitoVerificadorIncorreto() {
        Assertions.assertFalse(predicado.test("85800000004734504321701907161701904720320202"));
    }

    @Test
    void cenarioCodigoBarraComTerceiroDigitoVerificadorIncorreto() {
        Assertions.assertFalse(predicado.test("85800000004734504321701907161701904720320210"));
    }

}