package br.com.project.tax.payment.domain.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

class DatasTest {

    @Test
    void deveRetornar1Segundo() {

        final LocalDateTime agora = LocalDateTime.now();

        Assertions
                .assertEquals(1, Datas.differentInSeconds(agora.minusSeconds(1), agora));

    }

    @Test
    void deveTransformarEmLocalDate() {

        Assertions
                .assertEquals(LocalDate.of(2020, 9, 7), Datas.paraLocalDate("20200907"));
    }

    @Test
    void deveElevarExcessaoPorParseIncorretoEmLocalDate() {

        Assertions
                .assertThrows(DateTimeParseException.class, () -> Datas.paraLocalDate("y20200907"));

    }

    @Test
    void deveTransformarEmLocalTime() {

        Assertions
                .assertEquals(LocalTime.of(0, 50, 0), Datas.paraLocalTime("005000"));
    }

    @Test
    void deveElevarExcessaoPorParseIncorretoEmLocalTime() {

        Assertions
                .assertThrows(DateTimeParseException.class, () -> Datas.paraLocalTime("0050002"));

    }

    @Test
    void formatarDataParaString() {

        Assertions.assertEquals("20200907", Datas.formatarData(LocalDate.of(2020, 9, 7)));

    }

    @Test
    void formatarHoraParaString() {

        Assertions.assertEquals("005000", Datas.formatarHora(LocalTime.of(0, 50, 0)));

    }

    @Test
    void paraLocalDateTime() {

        Assertions
                .assertEquals
                        (
                                LocalDateTime
                                        .of(2020, 9, 7, 0, 50, 0),
                                Datas.paraDataHora("20200907", "005000")
                        );

    }
}