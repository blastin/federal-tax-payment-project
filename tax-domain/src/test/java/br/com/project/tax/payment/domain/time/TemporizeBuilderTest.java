package br.com.project.tax.payment.domain.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TemporizeBuilderTest {

    @Test
    void shouldCreateTemporize() {

        final long payment = 10L;

        final long init = 100L;

        final Temporize temporize = TemporizeBuilder.builder().toPayment(payment).initIs(init).build();

        Assertions
                .assertEquals(payment, temporize.payment());

        Assertions
                .assertEquals(init, temporize.init());

    }

    @Test
    void shouldReturnTemporizeMock() {

        final Temporize zero = Temporize.mock();

        Assertions
                .assertEquals(0L, zero.init());

        Assertions
                .assertEquals(0L, zero.payment());

    }
}