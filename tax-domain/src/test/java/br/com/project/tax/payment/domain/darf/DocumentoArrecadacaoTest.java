package br.com.project.tax.payment.domain.darf;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

class DocumentoArrecadacaoTest {

    @Test
    void deveRetornarDocumentoValido() {

        Assertions
                .assertTrue(DocumentoArrecadacao.documentoValido(DocumentoArrecadacao.mock().getCodigoDeBarra()));

    }

    @Test
    void deveRetornarDocumentoInvalido() {

        Assertions
                .assertFalse(DocumentoArrecadacao.documentoValido(""));

    }

    @Test
    void paraDocumentoNulodeveRetornarDocumentoInvalido() {

        Assertions
                .assertFalse(DocumentoArrecadacao.documentoValido(null));

    }

    @Test
    void deveRetornarDocumentoValidoQuandoTemosDeUmACinco() {

        Assertions
                .assertTrue(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("")));

        Assertions
                .assertTrue(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("", "")));

        Assertions
                .assertTrue(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("", "", "")));

        Assertions
                .assertTrue(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("", "", "", "")));

        Assertions
                .assertTrue(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("", "", "", "", "")));

    }

    @Test
    void deveRetornarDocumentoInvalidoQuandoTemosUmaVazia() {

        Assertions
                .assertFalse(DocumentoArrecadacao.quantidadeDocumentoValido(List.of()));
    }

    @Test
    void deveRetornarDocumentoInvalidoQuandoTemosMaisQueCinco() {

        Assertions
                .assertFalse(DocumentoArrecadacao.quantidadeDocumentoValido(List.of("", "", "", "", "", "")));

    }

    @Test
    void deveRetornarUmDocumentoArrecadacao() {

        final List<String> codigosDeBarra = List.of(DocumentoArrecadacao.mock().getCodigoDeBarra());

        final Set<DocumentoArrecadacao> boletoBancarios = DocumentoArrecadacao.de(codigosDeBarra);

        Assertions.assertFalse(boletoBancarios.isEmpty());

        Assertions.assertEquals(1, boletoBancarios.size());

        final DocumentoArrecadacao boletoBancario = boletoBancarios.iterator().next();

        Assertions.assertEquals(new BigDecimal("473.45"), boletoBancario.getValor());

        Assertions.assertEquals("07161701904720320", boletoBancario.getNumeroDocumento());

        Assertions.assertEquals(codigosDeBarra.get(0), boletoBancario.getCodigoDeBarra());

    }

}