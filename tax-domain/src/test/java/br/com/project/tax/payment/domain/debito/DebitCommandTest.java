package br.com.project.tax.payment.domain.debito;

import org.junit.jupiter.api.Test;

class DebitCommandTest {

    @Test
    void shouldBuildSkeletalDebitCommand() {

        final DebitCommand debitCommand = DebitCommand.mock();

        final Debit debit = Debit.mock();

        debitCommand.updateDebitSituation(debit.getProtocol(), DebitSituation.FALHA_PROCESSAMENTO);

        debitCommand.save(debit);

    }
}