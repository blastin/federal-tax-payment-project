package br.com.project.tax.payment.domain.tax;

import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class TaxRevenuesQueryTest {

    @Test
    void shouldReturnTaxRevenuesMock() {

        final TaxRevenuesQuery taxRevenuesQuery = TaxRevenuesQuery.mock();

        final Optional<TaxRevenues> optionalTaxRevenues = taxRevenuesQuery.recoveryByProtocol(Debit.mock().getProtocol());

        Assertions
                .assertTrue(optionalTaxRevenues.isPresent());

        final TaxRevenues taxRevenues = optionalTaxRevenues.get();

        Assertions
                .assertEquals(Debit.mock().getProtocol(), taxRevenues.getProtocol());

    }
}