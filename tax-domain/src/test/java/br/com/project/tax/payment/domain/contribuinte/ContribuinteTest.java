package br.com.project.tax.payment.domain.contribuinte;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContribuinteTest {

    @Test
    void deveRetornarCodigoIgual() {

        final int codigo = 1;

        Assertions
                .assertEquals(codigo, Contribuinte.builder().comTipo(codigo).build().getCodigo());

    }

    @Test
    void deveRetornarNumeroIgual() {

        final String numero = "";

        Assertions
                .assertEquals(numero, Contribuinte.builder().paraNumero(numero).build().getNumero());

    }

    @Test
    void deveRetornarContribuinteValido() {

        Assertions
                .assertTrue
                        (
                                Contribuinte.mock().valido()
                        );
    }

    @Test
    void deveRetornarContribuinteInvalido() {

        Assertions
                .assertFalse
                        (
                                Contribuinte
                                        .builder()
                                        .comTipo(1)
                                        .paraNumero("219.786.850-09")
                                        .build()
                                        .valido()
                        );

    }

    @Test
    void paraContribuinteComRegistroNulodeveRetornarContribuinteInvalido() {

        Assertions
                .assertFalse
                        (
                                Contribuinte
                                        .builder()
                                        .comTipo(1)
                                        .paraNumero(null)
                                        .build()
                                        .valido()
                        );

    }

}