package br.com.project.tax.payment.domain.debito;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class DebitQueryTest {

    @Test
    void shouldReturnDebit() {

        final DebitQuery debitQuery = DebitQuery.mock();

        final Debit debitMock = Debit.mock();

        final Optional<Debit> optionalDebit = debitQuery.recoveryByProtocol(debitMock.getProtocol());

        Assertions
                .assertTrue(optionalDebit.isPresent());

        final Debit debit = optionalDebit.get();

        Assertions
                .assertEquals(debitMock.getProtocol(), debit.getProtocol());

    }

    @Test
    void shouldReturnDebitExist() {

        final DebitQuery debitQuery = DebitQuery.mock();

        final boolean existsDebitByProtocol = debitQuery.existsDebitByProtocol(Debit.mock().getProtocol());

        Assertions.assertFalse(existsDebitByProtocol);

    }

}