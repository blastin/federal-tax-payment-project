package br.com.project.tax.payment.domain.time;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

class TimeoutTest {

    @Test
    @DisplayName("Should return time out not exceeded")
    void shouldReturnTimeOutNotExceeded() {

        Assertions
                .assertFalse
                        (
                                Timeout.mock()
                                        .exceeded()
                        );


    }

    @Test
    @DisplayName("Should return time out exceeded")
    void shouldReturnTimeOutExceeded() {

        final LocalDateTime threeSecondsBefore = LocalDateTime.now().minusSeconds(3);

        final Timeout timeout = Timeout.init(threeSecondsBefore, 2);

        Assertions
                .assertTrue
                        (
                                timeout
                                        .exceeded()
                        );

        Assertions
                .assertEquals(threeSecondsBefore.toString(), timeout.toString());

    }

    @Test
    @DisplayName("Should return exception IllegalArgumentException because time is future")
    void shouldReturnException() {

        final LocalDateTime dayAfter = LocalDateTime.now().plusSeconds(10);

        Assertions
                .assertThrows
                        (
                                IllegalArgumentException.class,
                                () ->

                                        Timeout
                                                .init(dayAfter, 0)
                        );

    }

}