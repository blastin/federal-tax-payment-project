package br.com.project.tax.payment.domain.tax;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TaxRevenuesGatewayTest {

    @Test
    void shouldReturnSkeletalImplementation() throws TaxRevenuesTransactionException {

        final TaxRevenuesGateway taxRevenuesGateway = TaxRevenuesGateway.mock();

        final TaxRevenues taxRevenues = TaxRevenues.mock();

        taxRevenuesGateway.reversal(taxRevenues.getAutenticationNumber());

        taxRevenuesGateway.save(taxRevenues);

        Assertions
                .assertTrue(taxRevenuesGateway.payFederalFee(TaxRevenuesPayment.mock()).isPresent());

    }
}