package br.com.project.tax.payment.domain.customer;

import br.com.project.tax.payment.domain.contribuinte.Contribuinte;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomerTest {

    private static final Customer CUSTOMER_MOCK = Customer.mock();

    @Test
    void bancoValido() {
        Assertions.assertTrue(Customer.bancoValido(CUSTOMER_MOCK.getCodigoBanco()));
    }

    @Test
    void bancoNulo() {
        Assertions.assertFalse(Customer.bancoValido(null));
    }

    @Test
    void bancoInvalidoComApenasCaracteresNaoNumericos() {
        final String banco = "ABC";
        Assertions.assertFalse(Customer.bancoValido(banco));
    }

    @Test
    void bancoInvalidoComUmNumeroAMenos() {
        final String banco = "01";
        Assertions.assertFalse(Customer.bancoValido(banco));
    }

    @Test
    void bancoInvalidoComUmNumeroAMais() {
        final String banco = "0123";
        Assertions.assertFalse(Customer.bancoValido(banco));
    }

    @Test
    void agenciaValida() {
        final String agencia = CUSTOMER_MOCK.getCodigoAgencia();
        Assertions.assertTrue(Customer.agenciaValida(agencia));
    }

    @Test
    void agenciaNula() {
        Assertions.assertFalse(Customer.agenciaValida(null));
    }

    @Test
    void agenciaInvalidaComUmCaracterNaoNumerico() {
        final String agencia = "012@";
        Assertions.assertFalse(Customer.agenciaValida(agencia));
    }

    @Test
    void agenciaInvalidaComUmNumeroAMais() {
        final String agencia = "01234";
        Assertions.assertFalse(Customer.agenciaValida(agencia));
    }

    @Test
    void agenciaInvalidaComUmNumeroAMenos() {
        final String agencia = "012";
        Assertions.assertFalse(Customer.agenciaValida(agencia));
    }

    @Test
    void contaCorrenteValida() {
        final String contaCorrente = CUSTOMER_MOCK.getContaCorrente();
        Assertions.assertTrue(Customer.contaCorrenteValida(contaCorrente));
    }

    @Test
    void contaCorrenteNula() {
        Assertions.assertFalse(Customer.contaCorrenteValida(null));
    }

    @Test
    void contaCorrenteValidaComApenasDoisValoresAntesDoDigitoVerificador() {
        final String contaCorrente = "1X-3";
        Assertions.assertTrue(Customer.contaCorrenteValida(contaCorrente));
    }

    @Test
    void contaCorrenteValidaSemMascara() {
        final String contaCorrente = "123453";
        Assertions.assertTrue(Customer.contaCorrenteValida(contaCorrente));
    }

    @Test
    void contaCorrenteInValidaComApenasUmNumeroAntesDoDigitoVerificador() {
        final String contaCorrente = "12";
        Assertions.assertFalse(Customer.contaCorrenteValida(contaCorrente));
    }

    @Test
    void contaCorrenteInValidaComApenasUmNumeroAMais() {
        final String contaCorrente = "1234567";
        Assertions.assertFalse(Customer.contaCorrenteValida(contaCorrente));
    }

    @Test
    void clienteDeveRetornarContribuinteIgual() {

        final Contribuinte contribuinte =
                Contribuinte
                        .builder()
                        .comTipo(1)
                        .build();

        Assertions
                .assertEquals
                        (
                                contribuinte,
                                Customer
                                        .builder()
                                        .comValorContribuinte(contribuinte)
                                        .build()
                                        .getContribuinte());

    }

}
