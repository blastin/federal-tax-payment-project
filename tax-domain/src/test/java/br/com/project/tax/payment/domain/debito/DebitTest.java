package br.com.project.tax.payment.domain.debito;

import br.com.project.tax.payment.domain.time.Datas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

class DebitTest {

    private static final Debit DEBIT_MOCK = Debit.mock();

    @Test
    void protocoloValido() {
        final String protocolo = DEBIT_MOCK.getProtocol();
        Assertions.assertTrue(Debit.protocoloValido(protocolo));
    }

    @Test
    void protocoloNulo() {
        Assertions.assertFalse(Debit.protocoloValido(null));
    }

    @Test
    void protocoloInvalidoCaracterNaoNumerico() {
        final String protocolo = "01234567890123456X";
        Assertions.assertFalse(Debit.protocoloValido(protocolo));
    }

    @Test
    void protocoloInvalidoNumeroAMais() {
        final String protocolo = "0123456789012345678";
        Assertions.assertFalse(Debit.protocoloValido(protocolo));
    }

    @Test
    void protocoloInvalidoNumeroAMenos() {
        final String protocolo = "01234567890123456";
        Assertions.assertFalse(Debit.protocoloValido(protocolo));
    }

    @Test
    void cpfDespachanteValido() {
        final String cpfDespachante = DEBIT_MOCK.getDespachante();
        Assertions.assertTrue(Debit.despachanteValido(cpfDespachante));
    }

    @Test
    void despachanteNulo() {
        Assertions.assertFalse(Debit.despachanteValido(null));
    }

    @Test
    void cpfDespachanteInvalido() {
        final String cpfDespachante = "499.589.630-93";
        Assertions.assertFalse(Debit.despachanteValido(cpfDespachante));
    }

    @Test
    void especieDebitoValidaPucomex() {
        final EspecieDebito especieDebito = DEBIT_MOCK.getEspecieDebito();
        Assertions.assertTrue(Debit.especieDebitoValida(especieDebito.toInt()));
    }

    @Test
    void especieDebitoValidaSiscomex() {
        Assertions.assertTrue(Debit.especieDebitoValida(EspecieDebito.SISCOMEX.toInt()));
    }

    @Test
    void especieDebitoInvalida() {
        final EspecieDebito especieDebito = EspecieDebito.OUTRO;
        Assertions.assertFalse(Debit.especieDebitoValida(especieDebito.toInt()));
    }

    @Test
    void referenciaDebitoValida() {
        final String referenciaDebito = DEBIT_MOCK.getReferenciaDebito();
        Assertions.assertTrue(Debit.referenciaDebitoValida(referenciaDebito));
    }

    @Test
    void referenciaDebitoNula() {
        Assertions.assertFalse(Debit.referenciaDebitoValida(null));
    }

    @Test
    void referenciaDebitoInvalidaComApenasNaoNumericos() {
        final String referenciaDebito = "ABCDEFGHIJ";
        Assertions.assertFalse(Debit.referenciaDebitoValida(referenciaDebito));
    }

    @Test
    void referenciaDebitoInvalidaComNumeroAMenos() {
        final String referenciaDebito = "012345678";
        Assertions.assertFalse(Debit.referenciaDebitoValida(referenciaDebito));
    }

    @Test
    void referenciaDebitoInvalidaComNumeroAMais() {
        final String referenciaDebito = "01234567890";
        Assertions.assertFalse(Debit.referenciaDebitoValida(referenciaDebito));
    }

    @Test
    void dataRequisicaoValida() {
        final String dataRequisicao = DEBIT_MOCK.getDataRequisicao();
        Assertions.assertTrue(Debit.dataRequisicaoValida(dataRequisicao));
    }

    @Test
    void dataRequisicaoNula() {
        Assertions.assertFalse(Debit.dataRequisicaoValida(null));
    }

    @Test
    void dataRequisicaoInvalida() {
        final String dataRequisicao = "2020-07-08";
        Assertions.assertFalse(Debit.dataRequisicaoValida(dataRequisicao));
    }

    @Test
    void dataRequisicaoSuperiorADataAtual() {
        final LocalDate localDate = LocalDate.now().plusDays(1);
        Assertions.assertFalse(Debit.dataRequisicaoValida(Datas.formatarData(localDate)));
    }

    @Test
    void horaRequisicaoValida() {
        final String horaRequisicao = DEBIT_MOCK.getHoraRequisicao();
        Assertions.assertTrue(Debit.horaRequisicaoValida(horaRequisicao));
    }

    @Test
    void horaRequisicaoNula() {
        Assertions.assertFalse(Debit.horaRequisicaoValida(null));
    }

    @Test
    void horaRequisicaoInvalida() {
        final String horaRequisicao = "15:00:00";
        Assertions.assertFalse(Debit.horaRequisicaoValida(horaRequisicao));
    }

    @Test
    void horaRequisicaoSuperiorADataAtual() {
        final LocalTime localTime = LocalTime.now().plusMinutes(1);
        Assertions.assertFalse(Debit.horaRequisicaoValida(Datas.formatarHora(localTime)));
    }

    @Test
    void deveRetornarCustoTotal() {
        Assertions.assertEquals(new BigDecimal("473.45"), DEBIT_MOCK.custoTotal());
    }

    @Test
    void deveRetornarConfirmado() {
        Assertions.assertEquals(DebitSituation.CONFIRMED, DEBIT_MOCK.getDebitSituation());
    }

}