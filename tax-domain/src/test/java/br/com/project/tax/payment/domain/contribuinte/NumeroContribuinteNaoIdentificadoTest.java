package br.com.project.tax.payment.domain.contribuinte;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class NumeroContribuinteNaoIdentificadoTest {

    @Test
    void deveRetornarFalsoParaQualquerParametroEntrada() {

        Assertions
                .assertFalse(TipoNumeroContribuinte.NAO_IDENTIFICADO.predicado().test(""));

    }
}
