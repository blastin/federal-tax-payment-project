package br.com.project.tax.payment.domain.tax;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class TaxRevenuesTransactionTest {

    @Test
    void shouldReturnTaxRevenues() throws TaxRevenuesTransactionException {

        final Optional<TaxRevenues> optionalTaxRevenuesAutentication
                = TaxRevenuesTransaction.mock().payFederalFee(TaxRevenuesPayment.mock());

        Assertions
                .assertTrue(optionalTaxRevenuesAutentication.isPresent());

    }

    @Test
    void shouldThrowTaxRevenuesTransactionException() {

        final TaxRevenuesTransaction taxRevenuesTransaction = new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
            @Override
            public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment)
                    throws TaxRevenuesTransactionException {
                throw new TaxRevenuesTransactionException("transaction error");
            }
        };

        Assertions
                .assertThrows
                        (
                                TaxRevenuesTransactionException.class,
                                () -> taxRevenuesTransaction.payFederalFee(TaxRevenuesPayment.mock())
                        );

    }

}