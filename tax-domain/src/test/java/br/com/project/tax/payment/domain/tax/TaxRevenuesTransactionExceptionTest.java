package br.com.project.tax.payment.domain.tax;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TaxRevenuesTransactionExceptionTest {

    @Test
    void shouldReturnTaxTevenuesSituation() {

        final String message = "message";

        final TaxRevenuesTransactionException taxRevenuesTransactionException =
                new TaxRevenuesTransactionException(message);

        Assertions
                .assertEquals(message, taxRevenuesTransactionException.getMessage());

    }
}