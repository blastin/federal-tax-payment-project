package br.com.project.tax.payment.domain.debito;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SituacaoDebitTest {

    @Test
    void deveRetornarPendente() {
        Assertions
                .assertEquals(DebitSituation.PENDENTE, DebitSituation.porCodigo(0));
    }

    @Test
    void deveRetornarConfirmado() {
        Assertions
                .assertEquals(DebitSituation.CONFIRMED, DebitSituation.porCodigo(1));
    }

    @Test
    void deveRetornarESTORNADO() {
        Assertions
                .assertEquals(DebitSituation.REVERSAL, DebitSituation.porCodigo(2));
    }

    @Test
    void deveRetornarREJEITADO() {
        Assertions
                .assertEquals(DebitSituation.INSUFFICIENT_FUNDS, DebitSituation.porCodigo(3));
    }

    @Test
    void deveRetornarNAO_ENCONTRADO() {
        Assertions
                .assertEquals(DebitSituation.NAO_ENCONTRADO, DebitSituation.porCodigo(4));
    }

    @Test
    void deveRetornarFALHA_PROCESSAMENTO() {
        Assertions
                .assertEquals(DebitSituation.FALHA_PROCESSAMENTO, DebitSituation.porCodigo(5));
    }

    @Test
    void deveRetornarINFORMACAO_INDISPONIVEL() {
        Assertions
                .assertEquals(DebitSituation.INFORMACAO_INDISPONIVEL, DebitSituation.porCodigo(99));
    }

}