package br.com.project.tax.payment.domain.debito;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EspecieDebitTest {

    @Test
    void shouldReturnPucomex() {

        final EspecieDebito pucomex = EspecieDebito.PUCOMEX;

        Assertions
                .assertEquals(pucomex, EspecieDebito.of(1));

        Assertions
                .assertEquals("pucomex", pucomex.toString());

    }

    @Test
    void shouldReturnSiscomex() {

        final EspecieDebito siscomex = EspecieDebito.SISCOMEX;

        Assertions
                .assertEquals(siscomex, EspecieDebito.of(2));

        Assertions
                .assertEquals("siscomex", siscomex.toString());

    }

    @Test
    void shouldReturnOutro() {

        final EspecieDebito outro = EspecieDebito.OUTRO;

        Assertions
                .assertEquals(outro, EspecieDebito.of(10034));

        Assertions
                .assertEquals("especie desconhecida", outro.toString());

    }

}