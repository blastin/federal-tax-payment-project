package br.com.project.tax.payment.domain.tax;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class TaxRevenuesSituationTest {

    @Test
    void shouldGetEnumerations() {

        Assertions.assertEquals(3, Arrays.stream(TaxRevenuesSituation.values()).count());

    }
}