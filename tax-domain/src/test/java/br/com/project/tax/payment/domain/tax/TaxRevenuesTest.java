package br.com.project.tax.payment.domain.tax;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class TaxRevenuesTest {

    @Test
    void shouldBuildTaxRevenuesWithTaxRevenuesPayment() {

        final TaxRevenues taxRevenues = TaxRevenues.builder(TaxRevenues.mock()).build();

        Assertions.assertNotNull(taxRevenues);

        final String protocol = Debit.mock().getProtocol();

        Assertions
                .assertEquals(protocol, taxRevenues.getAutenticationNumber());

        Assertions
                .assertNotNull(taxRevenues.getTaxRevenuesDate());


        Assertions
                .assertNotNull(taxRevenues.getTaxRevenuesTime());

        Assertions
                .assertEquals(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()), taxRevenues.getCodigosDeBarra());

        Assertions
                .assertEquals(protocol, taxRevenues.getProtocol());

    }

    @Test
    void shouldBuildTaxRevenuesWithTaxDebit() {

        final TaxRevenues taxRevenues = TaxRevenues.builder(TaxRevenues.mock()).withDebit(Debit.mock()).build();

        Assertions.assertNotNull(taxRevenues);

        final String protocol = Debit.mock().getProtocol();

        Assertions
                .assertEquals(protocol, taxRevenues.getAutenticationNumber());

        Assertions
                .assertNotNull(taxRevenues.getTaxRevenuesDate());


        Assertions
                .assertNotNull(taxRevenues.getTaxRevenuesTime());

        Assertions
                .assertEquals(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()), taxRevenues.getCodigosDeBarra());

        Assertions
                .assertEquals(protocol, taxRevenues.getProtocol());

    }

}