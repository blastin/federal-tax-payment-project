package br.com.project.tax.payment.domain.tax;

import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.time.Timeout;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

class TaxRevenuesPaymentTest {

    @Test
    void shouldBuildTaxRevenuesPayment() {

        final TaxRevenuesPayment taxRevenuesPayment = TaxRevenuesPayment.builder(TaxRevenuesPayment.mock()).build();

        final Debit debit = Debit.mock();

        final Customer customer = Customer.mock();

        Assertions
                .assertEquals(customer.getCodigoAgencia(), taxRevenuesPayment.getAgenciaBancaria());

        Assertions
                .assertEquals(customer.getCodigoBanco(), taxRevenuesPayment.getCodigoBanco());

        Assertions
                .assertEquals(customer.getContaCorrente(), taxRevenuesPayment.getContaCorrente());

        Assertions
                .assertEquals(debit.getProtocol(), taxRevenuesPayment.getProtocol());

        Assertions
                .assertEquals(debit.custoTotal(), taxRevenuesPayment.getCustoTotal());

        Assertions
                .assertEquals(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()), taxRevenuesPayment.getCodigosDeBarra());

    }

    @Test
    void shouldTimeOver() {

        final TaxRevenuesPayment taxRevenuesPayment =
                TaxRevenuesPayment
                        .builder()
                        .withCustomer(Customer.mock())
                        .toDebit(Debit.mock())
                        .withTimeOut(Timeout.init(LocalDateTime.now().minusSeconds(1), 0))
                        .withCodigosDeBarra(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()))
                        .build();

        Assertions
                .assertTrue(taxRevenuesPayment.timeOver());

    }


    @Test
    void shouldTimeNotOver() {

        final TaxRevenuesPayment taxRevenuesPayment =
                TaxRevenuesPayment
                        .builder()
                        .withCustomer(Customer.mock())
                        .toDebit(Debit.mock())
                        .withTimeOut(Timeout.init(LocalDateTime.now(), 10))
                        .withCodigosDeBarra(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()))
                        .build();

        Assertions
                .assertFalse(taxRevenuesPayment.timeOver());

    }
}