package br.com.project.tax.payment.domain.debito;

import java.util.stream.Stream;

public enum DebitSituation {

    PENDENTE(0, "Pagamento Pendente"),
    CONFIRMED(1, "Pagamento Confirmado"),
    REVERSAL(2, "Pagamento estornado"),
    INSUFFICIENT_FUNDS(3, "Pagamento não foi efetuado devido a falta de saldo"),
    NAO_ENCONTRADO(4, "Débito não encontrado"),
    FALHA_PROCESSAMENTO(5, "Falha de processamenton não permitiu efetuar o pagamento"),
    INFORMACAO_INDISPONIVEL(99, "Informação indisponível");

    public static DebitSituation porCodigo(final int codigo) {
        return Stream
                .of(DebitSituation.values())
                .filter(situacaoPagamento -> situacaoPagamento.toInt() == codigo)
                .findFirst()
                .orElse(DebitSituation.INFORMACAO_INDISPONIVEL);
    }

    DebitSituation(final int codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    private final String descricao;

    private final int codigo;

    public int toInt() {
        return codigo;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
