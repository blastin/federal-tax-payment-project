package br.com.project.tax.payment.domain.darf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DocumentoArrecadacao {

    private static final Pattern padraoCodigoDeBarra = Pattern.compile("^\\d{44}$");

    private static final Predicate<String> PREDICADO_CODIGO_DE_BARRA = new PredicadoCodigoDeBarra();

    public static boolean documentoValido(final String documento) {

        if (documento == null) return false;

        if (padraoCodigoDeBarra.matcher(documento).matches()) return PREDICADO_CODIGO_DE_BARRA.test(documento);

        return false;

    }

    public static boolean quantidadeDocumentoValido(final List<String> documentos) {

        if (documentos == null) return false;

        final int tamanho = documentos.size();

        return tamanho > 0 && tamanho <= 5;

    }

    public static Set<DocumentoArrecadacao> de(final List<String> codigosDeBarra) {
        return codigosDeBarra.stream().map(DocumentoArrecadacao::new).collect(Collectors.toSet());
    }

    public static DocumentoArrecadacao de(final String codigoDeBarra) {
        return new DocumentoArrecadacao(codigoDeBarra);
    }

    public static DocumentoArrecadacao mock() {
        return new DocumentoArrecadacao("85870000004734504321701907161701904720320212");
    }

    private DocumentoArrecadacao(final String codigoDeBarra) {

        this.codigoDeBarra = codigoDeBarra;

        numeroDocumento = codigoDeBarra.substring(24, 41);

        final String valorFormatoString = codigoDeBarra.substring(4, 15);

        valor = new BigDecimal(valorFormatoString).divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);

    }

    private final String codigoDeBarra;
    private final String numeroDocumento;
    private final BigDecimal valor;

    public String getCodigoDeBarra() {
        return codigoDeBarra;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public BigDecimal getValor() {
        return valor;
    }

}
