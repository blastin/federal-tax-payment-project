package br.com.project.tax.payment.domain.time;

public class TemporizeBuilder {

    static final Temporize ZERO = builder().initIs(0).toPayment(0).build();

    public static final Temporize FIVE = builder().initIs(5).toPayment(5).build();

    public static TemporizeBuilder builder() {
        return new TemporizeBuilder();
    }

    private TemporizeBuilder() {
    }

    private long valueInit;

    private long valuePayment;

    public TemporizeBuilder initIs(final long init) {
        valueInit = init;
        return this;
    }

    public TemporizeBuilder toPayment(final long payment) {
        valuePayment = payment;
        return this;
    }

    public Temporize build() {
        return new Temporize() {
            @Override
            public long init() {
                return valueInit;
            }

            @Override
            public long payment() {
                return valuePayment;
            }
        };
    }
}
