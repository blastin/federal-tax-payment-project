package br.com.project.tax.payment.domain.tax;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface TaxRevenuesGateway extends TaxRevenuesTransaction, TaxRevenuesCommand, TaxRevenuesQuery {

    class SkeletalTaxRevenuesGateway implements TaxRevenuesGateway {

        public SkeletalTaxRevenuesGateway() {
            taxRevenuesTransaction = TaxRevenuesTransaction.mock();
            taxRevenuesCommand = TaxRevenuesCommand.mock();
            taxRevenuesQuery = TaxRevenuesQuery.mock();
        }

        private final TaxRevenuesTransaction taxRevenuesTransaction;

        private final TaxRevenuesCommand taxRevenuesCommand;

        private final TaxRevenuesQuery taxRevenuesQuery;

        @Override
        public void save(final TaxRevenues taxRevenues) {
            taxRevenuesCommand.save(taxRevenues);
        }

        @Override
        public void reversal(final String autenticationNumber) {
            taxRevenuesTransaction.reversal(autenticationNumber);
        }

        @Override
        public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment)
                throws TaxRevenuesTransactionException {
            return taxRevenuesTransaction.payFederalFee(taxRevenuesPayment);
        }

        @Override
        public Optional<TaxRevenues> recoveryByProtocol(final String protocol) {
            return taxRevenuesQuery.recoveryByProtocol(protocol);
        }

    }

    static TaxRevenuesGateway mock() {
        return new SkeletalTaxRevenuesGateway();
    }

}
