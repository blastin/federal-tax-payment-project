package br.com.project.tax.payment.domain.debito;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface DebitGateway extends DebitCommand, DebitQuery {

    class SkeletalDebitGateway implements DebitGateway {

        private final DebitCommand debitCommand;

        private final DebitQuery debitQuery;

        public SkeletalDebitGateway() {
            debitQuery = DebitQuery.mock();
            debitCommand = DebitCommand.mock();
        }

        @Override
        public void save(final Debit debit) {
            debitCommand.save(debit);
        }

        @Override
        public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
            debitCommand.updateDebitSituation(protocol, debitSituation);
        }

        @Override
        public boolean existsDebitByProtocol(final String protocol) {
            return debitQuery.existsDebitByProtocol(protocol);
        }

        @Override
        public Optional<Debit> recoveryByProtocol(final String protocol) {
            return debitQuery.recoveryByProtocol(protocol);
        }
    }

    static DebitGateway mock() {
        return new SkeletalDebitGateway();
    }

}
