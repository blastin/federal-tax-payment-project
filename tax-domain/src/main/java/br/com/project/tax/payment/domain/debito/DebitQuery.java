package br.com.project.tax.payment.domain.debito;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface DebitQuery {

    class SkeletalDebitQuery implements DebitQuery {

        @Override
        public boolean existsDebitByProtocol(String protocol) {
            return false;
        }

        @Override
        public Optional<Debit> recoveryByProtocol(String protocol) {
            return Optional.of(Debit.mock());
        }

    }

    static DebitQuery mock() {
        return new SkeletalDebitQuery();
    }

    boolean existsDebitByProtocol(final String protocol);

    Optional<Debit> recoveryByProtocol(final String protocol);

}
