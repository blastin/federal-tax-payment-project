package br.com.project.tax.payment.domain.debito;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface DebitCommand {

    static DebitCommand mock() {
        return new SkeletalDebitCommand();
    }

    class SkeletalDebitCommand implements DebitCommand {

        @Override
        public void save(final Debit debit) {
            // save
        }

        @Override
        public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
            // update
        }
    }

    void save(final Debit debit);

    void updateDebitSituation(final String protocol, final DebitSituation debitSituation);

}
