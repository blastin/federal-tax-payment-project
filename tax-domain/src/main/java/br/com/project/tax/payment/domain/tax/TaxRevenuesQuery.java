package br.com.project.tax.payment.domain.tax;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface TaxRevenuesQuery {

    static TaxRevenuesQuery mock() {
        return protocol -> Optional.of(TaxRevenues.mock());
    }

    Optional<TaxRevenues> recoveryByProtocol(final String protocol);

}
