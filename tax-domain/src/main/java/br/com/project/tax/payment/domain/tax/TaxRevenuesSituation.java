package br.com.project.tax.payment.domain.tax;

public enum TaxRevenuesSituation {

    PAID,
    NOT_PAID,
    PAYMENT_ERROR

}
