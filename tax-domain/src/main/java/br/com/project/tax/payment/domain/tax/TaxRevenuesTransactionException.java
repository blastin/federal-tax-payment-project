package br.com.project.tax.payment.domain.tax;

public class TaxRevenuesTransactionException extends Exception {

    public TaxRevenuesTransactionException(final String message) {
        super(message);
    }

}
