package br.com.project.tax.payment.domain.debito;

import br.com.project.tax.payment.domain.contribuinte.TipoNumeroContribuinte;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.time.Datas;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Set;
import java.util.regex.Pattern;

public class Debit {

    private static final Pattern PROTOCOL_PATTERN = Pattern.compile("^\\d{18}$");

    public static boolean protocoloValido(final String protocol) {
        if (protocol == null) return false;
        return PROTOCOL_PATTERN.matcher(protocol).matches();
    }

    public static boolean despachanteValido(final String despachante) {
        return TipoNumeroContribuinte.CPF.predicado().test(despachante);
    }

    public static boolean especieDebitoValida(final int especieDebito) {

        switch (EspecieDebito.of(especieDebito)) {
            case PUCOMEX:
            case SISCOMEX:
                return true;
            default:
                return false;
        }

    }

    private static final Pattern padraoReferenciaDebito = Pattern.compile("^\\d{10}$");

    public static boolean referenciaDebitoValida(final String referenciaDebito) {
        if (referenciaDebito == null) return false;
        return padraoReferenciaDebito.matcher(referenciaDebito).matches();
    }

    public static boolean dataRequisicaoValida(final String dataRequisicao) {

        if (dataRequisicao == null) return false;

        try {
            return !Datas.paraLocalDate(dataRequisicao).isAfter(LocalDate.now());
        } catch (DateTimeParseException dt) {
            return false;
        }

    }

    public static boolean horaRequisicaoValida(final String horaRequisicao) {

        if (horaRequisicao == null) return false;

        try {
            return !Datas.paraLocalTime(horaRequisicao).isAfter(LocalTime.now());
        } catch (DateTimeParseException dt) {
            return false;
        }

    }

    public static Debit mock() {

        final LocalDateTime agora = LocalDateTime.now();

        return
                builder()
                        .comProtocolo("012345678901234567")
                        .comReferencia("0123456789")
                        .feitaPeloDespachante("499.589.630-94")
                        .paraData(Datas.formatarData(agora.toLocalDate()))
                        .paraHora(Datas.formatarHora(agora.toLocalTime()))
                        .paraEspecie(EspecieDebito.SISCOMEX)
                        .situacaoDebito(DebitSituation.CONFIRMED)
                        .comArrecadacoes(Set.of(DocumentoArrecadacao.mock()))
                        .build();

    }

    public static DebitoConstrutor builder() {
        return new DebitoConstrutor();
    }

    public static class DebitoConstrutor {

        private DebitoConstrutor() {
        }

        private String protocolo;

        private EspecieDebito especieDebito;

        private String referenciaDebito;

        private String dataRequisicao;

        private String horaRequisicao;

        private String despachante;

        private Set<DocumentoArrecadacao> arrecadacoes;

        private DebitSituation debitSituation = DebitSituation.PENDENTE;

        public DebitoConstrutor comProtocolo(final String protocolo) {
            this.protocolo = protocolo;
            return this;
        }

        public DebitoConstrutor paraEspecie(final EspecieDebito especieDebito) {
            this.especieDebito = especieDebito;
            return this;
        }

        public DebitoConstrutor comReferencia(final String referenciaDebito) {
            this.referenciaDebito = referenciaDebito;
            return this;
        }

        public DebitoConstrutor paraData(final String dataRequisicao) {
            this.dataRequisicao = dataRequisicao;
            return this;
        }

        public DebitoConstrutor paraHora(final String horaRequisicao) {
            this.horaRequisicao = horaRequisicao;
            return this;
        }

        public DebitoConstrutor feitaPeloDespachante(final String despachante) {
            this.despachante = despachante;
            return this;
        }

        public DebitoConstrutor comArrecadacoes(final Set<DocumentoArrecadacao> arrecadacoes) {
            this.arrecadacoes = arrecadacoes;
            return this;
        }

        public DebitoConstrutor situacaoDebito(final DebitSituation debitSituation) {
            this.debitSituation = debitSituation;
            return this;
        }

        public Debit build() {
            return new Debit(this);
        }

    }

    private Debit(final DebitoConstrutor debitoConstrutor) {
        protocol = debitoConstrutor.protocolo;
        especieDebito = debitoConstrutor.especieDebito;
        referenciaDebito = debitoConstrutor.referenciaDebito;
        dataRequisicao = debitoConstrutor.dataRequisicao;
        horaRequisicao = debitoConstrutor.horaRequisicao;
        despachante = debitoConstrutor.despachante;
        arrecadacoes = debitoConstrutor.arrecadacoes;
        debitSituation = debitoConstrutor.debitSituation;
    }

    private final String protocol;

    private final EspecieDebito especieDebito;

    private final String referenciaDebito;

    private final String dataRequisicao;

    private final String horaRequisicao;

    private final String despachante;

    private final Set<DocumentoArrecadacao> arrecadacoes;

    private final DebitSituation debitSituation;

    public String getProtocol() {
        return protocol;
    }

    public EspecieDebito getEspecieDebito() {
        return especieDebito;
    }

    public String getReferenciaDebito() {
        return referenciaDebito;
    }

    public String getDataRequisicao() {
        return dataRequisicao;
    }

    public String getHoraRequisicao() {
        return horaRequisicao;
    }

    public String getDespachante() {
        return despachante;
    }

    public Set<DocumentoArrecadacao> getArrecadacoes() {
        return arrecadacoes;
    }

    public BigDecimal custoTotal() {
        return getArrecadacoes()
                .stream()
                .map(DocumentoArrecadacao::getValor)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public DebitSituation getDebitSituation() {
        return debitSituation;
    }

}

