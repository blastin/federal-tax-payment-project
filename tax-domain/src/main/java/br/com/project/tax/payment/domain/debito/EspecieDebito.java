package br.com.project.tax.payment.domain.debito;

import java.util.stream.Stream;

public enum EspecieDebito {

    PUCOMEX(1, "pucomex"),
    SISCOMEX(2, "siscomex"),
    OUTRO(99, "especie desconhecida");

    public static EspecieDebito of(final int codigo) {

        return Stream
                .of(EspecieDebito.values())
                .filter(especieDebito -> especieDebito.toInt() == codigo)
                .findFirst().orElse(OUTRO);

    }

    EspecieDebito(int codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    private final int codigo;

    private final String descricao;

    public int toInt() {
        return codigo;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
