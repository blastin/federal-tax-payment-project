package br.com.project.tax.payment.domain.tax;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface TaxRevenuesCommand {

    static TaxRevenuesCommand mock() {
        return taxRevenues -> {
        };
    }

    void save(final TaxRevenues taxRevenues);

}
