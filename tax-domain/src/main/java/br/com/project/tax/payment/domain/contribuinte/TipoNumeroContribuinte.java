package br.com.project.tax.payment.domain.contribuinte;

import java.util.function.Predicate;
import java.util.stream.Stream;

public enum TipoNumeroContribuinte {

    CNPJ(1) {
        @Override
        public Predicate<String> predicado() {
            return new NumeroContribuinte.PredicadoCNPJ();
        }
    },
    CPF(2) {
        @Override
        public Predicate<String> predicado() {
            return new NumeroContribuinte.PredicadoCPF();
        }
    },
    NAO_IDENTIFICADO(3) {
        @Override
        public Predicate<String> predicado() {
            return s -> false;
        }
    };

    static TipoNumeroContribuinte of(final int codigo) {

        return Stream
                .of(TipoNumeroContribuinte.values())
                .filter(tipoNumeroContribuinte -> tipoNumeroContribuinte.codigo == codigo)
                .findAny()
                .orElse(NAO_IDENTIFICADO);

    }

    TipoNumeroContribuinte(final int codigo) {
        this.codigo = codigo;
    }

    private final int codigo;

    public int getCodigo() {
        return codigo;
    }

    public abstract Predicate<String> predicado();

}
