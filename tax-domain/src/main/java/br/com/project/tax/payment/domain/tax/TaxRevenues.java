package br.com.project.tax.payment.domain.tax;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.time.Datas;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TaxRevenues {

    public static TaxRevenuesBuilder builder() {
        return new TaxRevenuesBuilder();
    }

    public static TaxRevenuesBuilder builder(final TaxRevenues taxRevenues) {
        return new TaxRevenuesBuilder(taxRevenues);
    }

    public static class TaxRevenuesBuilder {

        private TaxRevenuesBuilder() {
            codigosDeBarra = new ArrayList<>();
        }

        private TaxRevenuesBuilder(final TaxRevenues taxRevenues) {
            autenticationNumber = taxRevenues.autenticationNumber;
            taxRevenuesDate = taxRevenues.taxRevenuesDate;
            taxRevenuesTime = taxRevenues.taxRevenuesTime;
            protocol = taxRevenues.getProtocol();
            codigosDeBarra = taxRevenues.getCodigosDeBarra();
        }

        private String autenticationNumber;

        private LocalDate taxRevenuesDate;

        private LocalTime taxRevenuesTime;

        private String protocol;

        private List<String> codigosDeBarra;

        public TaxRevenuesBuilder withNumber(final String autenticationNumber) {
            this.autenticationNumber = autenticationNumber;
            return this;
        }

        public TaxRevenuesBuilder taxRevenuesPaymentInDate(final String taxRevenuesDate, final DateTimeFormatter dateTimeFormatter) {
            this.taxRevenuesDate = Datas.paraLocalDate(taxRevenuesDate, dateTimeFormatter);
            return this;
        }

        public TaxRevenuesBuilder taxRevenuesPaymentInTime(final String taxRevenuesTime, final DateTimeFormatter dateTimeFormatter) {
            this.taxRevenuesTime = Datas.paraLocalTime(taxRevenuesTime, dateTimeFormatter);
            return this;
        }

        public TaxRevenuesBuilder protocolIs(final String protocol) {
            this.protocol = protocol;
            return this;
        }

        public TaxRevenuesBuilder withPayment(final TaxRevenuesPayment taxRevenuesPayment) {

            codigosDeBarra = taxRevenuesPayment.getCodigosDeBarra();

            return protocolIs(taxRevenuesPayment.getProtocol());

        }

        public TaxRevenuesBuilder withDebit(final Debit debit) {

            codigosDeBarra =
                    debit
                            .getArrecadacoes()
                            .stream()
                            .map(DocumentoArrecadacao::getCodigoDeBarra)
                            .collect(Collectors.toUnmodifiableList());


            return protocolIs(debit.getProtocol());

        }

        public TaxRevenues build() {
            return new TaxRevenues(this);
        }

    }

    public static TaxRevenues mock() {

        final LocalDateTime now = LocalDateTime.now();

        return builder()
                .withNumber(Debit.mock().getProtocol())
                .taxRevenuesPaymentInTime(now.toLocalTime().toString(), DateTimeFormatter.ISO_TIME)
                .taxRevenuesPaymentInDate(now.toLocalDate().toString(), DateTimeFormatter.ISO_DATE)
                .withPayment(TaxRevenuesPayment.mock())
                .build();

    }

    private TaxRevenues(final TaxRevenuesBuilder builder) {
        autenticationNumber = builder.autenticationNumber;
        taxRevenuesDate = builder.taxRevenuesDate;
        taxRevenuesTime = builder.taxRevenuesTime;
        protocol = builder.protocol;
        codigosDeBarra = builder.codigosDeBarra;
    }

    private final String autenticationNumber;

    private final LocalDate taxRevenuesDate;

    private final LocalTime taxRevenuesTime;

    private final String protocol;

    private final List<String> codigosDeBarra;

    public String getAutenticationNumber() {
        return autenticationNumber;
    }

    public LocalDate getTaxRevenuesDate() {
        return taxRevenuesDate;
    }

    public LocalTime getTaxRevenuesTime() {
        return taxRevenuesTime;
    }

    public List<String> getCodigosDeBarra() {
        return codigosDeBarra;
    }

    public String getProtocol() {
        return protocol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxRevenues that = (TaxRevenues) o;
        return Objects.equals(getAutenticationNumber(), that.getAutenticationNumber()) &&
                Objects.equals(getTaxRevenuesDate(), that.getTaxRevenuesDate()) &&
                Objects.equals(getTaxRevenuesTime(), that.getTaxRevenuesTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAutenticationNumber(), getTaxRevenuesDate(), getTaxRevenuesTime());
    }

}

