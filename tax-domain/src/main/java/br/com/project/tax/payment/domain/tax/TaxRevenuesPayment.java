package br.com.project.tax.payment.domain.tax;

import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.time.Timeout;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TaxRevenuesPayment {

    public static TaxRevenuesPaymentBuilder builder() {
        return new TaxRevenuesPaymentBuilder();
    }

    public static TaxRevenuesPaymentBuilder builder(final TaxRevenuesPayment taxRevenuesPayment) {
        return new TaxRevenuesPaymentBuilder(taxRevenuesPayment);
    }

    public static class TaxRevenuesPaymentBuilder {

        private TaxRevenuesPaymentBuilder() {
            codigosDeBarra = new ArrayList<>();
        }

        private TaxRevenuesPaymentBuilder(final TaxRevenuesPayment taxRevenuesPayment) {
            customer = taxRevenuesPayment.customer;
            debit = taxRevenuesPayment.debit;
            timeout = taxRevenuesPayment.timeout;
            codigosDeBarra = taxRevenuesPayment.codigosDeBarra;
        }

        private Customer customer;
        private Debit debit;
        private Timeout timeout;

        private final List<String> codigosDeBarra;

        public TaxRevenuesPaymentBuilder withCustomer(final Customer customer) {
            this.customer = customer;
            return this;
        }

        public TaxRevenuesPaymentBuilder toDebit(final Debit debit) {
            this.debit = debit;
            return this;
        }

        public TaxRevenuesPaymentBuilder withTimeOut(final Timeout timeOut) {
            this.timeout = timeOut;
            return this;
        }

        public TaxRevenuesPaymentBuilder withCodigosDeBarra(final List<String> codigosDeBarra) {
            this.codigosDeBarra.addAll(codigosDeBarra);
            return this;
        }

        public TaxRevenuesPayment build() {
            return new TaxRevenuesPayment(this);
        }

    }

    public static TaxRevenuesPayment mock() {

        return builder()
                .toDebit(Debit.mock())
                .withCustomer(Customer.mock())
                .withTimeOut(Timeout.mock())
                .withCodigosDeBarra(List.of(DocumentoArrecadacao.mock().getCodigoDeBarra()))
                .build();

    }

    private TaxRevenuesPayment(final TaxRevenuesPaymentBuilder taxRevenuesPaymentBuilder) {
        customer = taxRevenuesPaymentBuilder.customer;
        debit = taxRevenuesPaymentBuilder.debit;
        timeout = taxRevenuesPaymentBuilder.timeout;
        codigosDeBarra = taxRevenuesPaymentBuilder.codigosDeBarra;
    }

    private final List<String> codigosDeBarra;

    private final Timeout timeout;

    private final Customer customer;

    private final Debit debit;

    public String getProtocol() {
        return debit.getProtocol();
    }

    public String getCodigoBanco() {
        return customer.getCodigoBanco();
    }

    public String getAgenciaBancaria() {
        return customer.getCodigoAgencia();
    }

    public String getContaCorrente() {
        return customer.getContaCorrente();
    }

    public BigDecimal getCustoTotal() {
        return debit.custoTotal();
    }

    public List<String> getCodigosDeBarra() {
        return codigosDeBarra;
    }

    public boolean timeOver() {
        return timeout.exceeded();
    }

}
