package br.com.project.tax.payment.domain.customer;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface CustomerQuery {

    boolean existsCostumer(final Customer customer);

}
