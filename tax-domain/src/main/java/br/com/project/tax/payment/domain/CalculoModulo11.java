package br.com.project.tax.payment.domain;

public class CalculoModulo11 {

    private static final int MODULO_NUMERO_CONTRIBUINTE = 11;

    public static int[] transformarCadeiaDeCaracteresEmUmArray(final String cadeiaCaracteres) {

        return cadeiaCaracteres
                .chars()
                .map(Character::getNumericValue)
                .filter(CalculoModulo11::valoresNoIntervalorDe0A9)
                .toArray();

    }

    public static boolean digitoVerificadorIncorreto(final int indiceDigito, final int[] cadeiaNumerica, final int possivelDigitoVerificador) {
        return possivelDigitoVerificador != cadeiaNumerica[indiceDigito - 1];
    }

    public static int calcularDigitoVerificador(final int[] cadeiaNumerica, final int base, final int limite) {

        final int resultado = somarPosicoesDeCadeiaNumerica(cadeiaNumerica, base, limite);

        return calcularDigitoPorRestricaoDeBase(resultado);

    }

    private static int somarPosicoesDeCadeiaNumerica(final int[] cadeiaNumerica, final int base, final int limite) {

        int somatoria = 0;

        for (int indice = limite - 1, fatorIncremental = 0; indice >= 0; indice--, fatorIncremental++) {

            final int fatorMultiplicador = proximoFatorMultiplicador(base, fatorIncremental);

            somatoria += cadeiaNumerica[indice] * fatorMultiplicador;

        }

        return somatoria;

    }

    private static boolean valoresNoIntervalorDe0A9(final int valor) {
        return valor >= 0 && valor <= 9;
    }

    private static int proximoFatorMultiplicador(final int base, final int fatorIncremental) {
        return 2 + fatorIncremental % base;
    }

    private static int calcularDigitoPorRestricaoDeBase(final int resultado) {

        final int resto = resultado % MODULO_NUMERO_CONTRIBUINTE;

        if (resto < 2) return 0;

        return MODULO_NUMERO_CONTRIBUINTE - resto;

    }

    private CalculoModulo11() {
    }

}
