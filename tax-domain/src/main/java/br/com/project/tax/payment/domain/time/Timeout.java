package br.com.project.tax.payment.domain.time;

import java.time.LocalDateTime;

/**
 * Timeout representa uma maquina de estado para estipular tempo máximo .
 * <p>
 * Dois estados são posssíveis : tempo não excedido e tempo limite atingido
 *
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public class Timeout {

    public static Timeout mock() {
        return init(LocalDateTime.now(), 2L);
    }

    /**
     * Caso argumento de entrada LocalDateTime seja posterior a data atual,
     * uma exceção IllegalArgumentException será levantada
     *
     * @param tempo     instância de LocalDateTime
     * @param temporize tempo em segundo total
     * @return uma nova instância de Temporizador
     */
    public static Timeout init(final LocalDateTime tempo, final long temporize) {

        final LocalDateTime agora = LocalDateTime.now();

        if (tempo.isAfter(agora)) throw new IllegalArgumentException("should not be after date actual");

        return new Timeout(tempo, temporize);

    }

    private Timeout(final LocalDateTime tempo, long temporize) {
        this.tempo = tempo;
        this.temporize = temporize;
    }

    private final LocalDateTime tempo;

    private final long temporize;

    /**
     * É realizado cálculo da diferença entre duas datas e feito uma operação lógica comparável entre os temporize
     *
     * @return caso tempo limite seja atingido retornará true
     */
    public boolean exceeded() {

        final long diffSeconds = Datas.differentInSeconds(tempo, LocalDateTime.now());

        return diffSeconds >= temporize;

    }

    @Override
    public String toString() {
        return tempo.toString();
    }

}

