package br.com.project.tax.payment.domain.tax;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface TaxRevenuesTransaction {

    class SkeletalTaxRevenuesTransaction implements TaxRevenuesTransaction {

        @Override
        public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment) throws TaxRevenuesTransactionException {
            return Optional.of(TaxRevenues.mock());
        }

        @Override
        public void reversal(String autenticationNumber) {
            // reversal
        }

    }

    static TaxRevenuesTransaction mock() {
        return new SkeletalTaxRevenuesTransaction();
    }

    Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment)
            throws TaxRevenuesTransactionException;

    void reversal(final String autenticationNumber);

}
