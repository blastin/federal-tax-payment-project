package br.com.project.tax.payment.domain.time;

public interface Temporize {

    static Temporize mock() {
        return TemporizeBuilder.ZERO;
    }

    long init();

    long payment();

}
