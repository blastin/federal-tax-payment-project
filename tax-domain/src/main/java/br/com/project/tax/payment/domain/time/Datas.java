package br.com.project.tax.payment.domain.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

public class Datas {

    private static final DateTimeFormatter PADRAO_DATA = DateTimeFormatter.ofPattern("yyyyMMdd");

    public static LocalDate paraLocalDate(final String data) {
        return paraLocalDate(data, PADRAO_DATA);
    }

    public static LocalDate paraLocalDate(final String taxRevenuesDate, final DateTimeFormatter dateTimeFormatter) {
        return LocalDate.parse(taxRevenuesDate, dateTimeFormatter);
    }

    public static String formatarData(final LocalDate data) {
        return data.format(PADRAO_DATA);
    }

    private static final DateTimeFormatter PADRAO_HORA = DateTimeFormatter.ofPattern("HHmmss");

    public static LocalTime paraLocalTime(String hora) {
        return paraLocalTime(hora, PADRAO_HORA);
    }

    public static LocalTime paraLocalTime(String hora, DateTimeFormatter padraoHora) {
        return LocalTime.parse(hora, padraoHora);
    }

    public static String formatarHora(final LocalTime hora) {
        return hora.format(PADRAO_HORA);
    }

    public static LocalDateTime paraDataHora(final String data, final String hora) {
        return LocalDateTime.of(paraLocalDate(data), paraLocalTime(hora));
    }

    static long differentInSeconds(Temporal initialTime, Temporal finalTime) {
        return ChronoUnit.SECONDS.between(initialTime, finalTime);
    }

    private Datas() {
    }

}
