package br.com.project.tax.payment.domain.customer;

import br.com.project.tax.payment.domain.contribuinte.Contribuinte;

import java.util.regex.Pattern;

public final class Customer {

    private static final Pattern padraoCodigoBanco = Pattern.compile("^\\d{3}$");

    public static boolean bancoValido(final String banco) {
        if (banco == null) return false;
        return padraoCodigoBanco.matcher(banco).matches();
    }

    private static final Pattern padraoAgencia = Pattern.compile("^\\d{4}$");

    public static boolean agenciaValida(final String agencia) {
        if (agencia == null) return false;
        return padraoAgencia.matcher(agencia).matches();
    }

    private static final Pattern padraoContaCorrente = Pattern.compile("^\\w{2,5}-?\\w$");

    public static boolean contaCorrenteValida(final String contaCorrente) {
        if (contaCorrente == null) return false;
        return padraoContaCorrente.matcher(contaCorrente).matches();
    }

    public static ClienteConstrutor builder() {
        return new ClienteConstrutor();
    }

    public static class ClienteConstrutor {

        private ClienteConstrutor() {
        }

        private Contribuinte contribuinte;

        private String contaCorrente;
        private String codigoAgencia;
        private String codigoBanco;

        public ClienteConstrutor comValorContribuinte(final Contribuinte contribuinte) {
            this.contribuinte = contribuinte;
            return this;
        }

        public ClienteConstrutor paraContaCorrente(final String contaCorrente) {
            this.contaCorrente = contaCorrente;
            return this;
        }

        public ClienteConstrutor contemCodigoAgencia(final String codigoAgencia) {
            this.codigoAgencia = codigoAgencia;
            return this;
        }

        public ClienteConstrutor paraBanco(final String codigoBanco) {
            this.codigoBanco = codigoBanco;
            return this;
        }

        public Customer build() {
            return new Customer(this);
        }

    }

    public static Customer mock() {

        return
                Customer
                        .builder()
                        .comValorContribuinte(Contribuinte.mock())
                        .contemCodigoAgencia("0123")
                        .paraContaCorrente("12345-3")
                        .paraBanco("012")
                        .build();

    }

    private Customer(final ClienteConstrutor clienteConstrutor) {
        contribuinte = clienteConstrutor.contribuinte;
        contaCorrente = clienteConstrutor.contaCorrente;
        codigoAgencia = clienteConstrutor.codigoAgencia;
        codigoBanco = clienteConstrutor.codigoBanco;
    }

    private final Contribuinte contribuinte;

    private final String contaCorrente;
    private final String codigoAgencia;
    private final String codigoBanco;

    public Contribuinte getContribuinte() {
        return contribuinte;
    }

    public String getContaCorrente() {
        return contaCorrente;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

}
