package br.com.project.tax.payment.domain.darf;

import br.com.project.tax.payment.domain.CalculoModulo11;

import java.util.function.Predicate;

final class PredicadoCodigoDeBarra implements Predicate<String> {

    private static final int INDICE_DIGITO_VERIFICADOR = 4;

    private static final int INDICE_PRIMEIRO_DIGITO_VERIFICADOR = 42;

    private static final int INDICE_SEGUNDO_DIGITO_VERIFICADOR = 43;

    private static final int INDICE_TERCEIRO_DIGITO_VERIFICADOR = 44;

    private static final int DIGITOS_EXCLUIDOS = 1;

    private static boolean digitosVerificadoresValidos(final int[] codigoDeBarra) {

        final int[] codigoDeBarraFiltrado = removerIndiceQuatroDoCodigoDeBarra(codigoDeBarra.clone());

        final int digitoVerificadorGeral = CalculoModulo11
                .calcularDigitoVerificador(codigoDeBarraFiltrado, 8, 44 - DIGITOS_EXCLUIDOS);

        if (CalculoModulo11.digitoVerificadorIncorreto(INDICE_DIGITO_VERIFICADOR, codigoDeBarra, digitoVerificadorGeral))
            return false;

        return digitosVerificadoresOpcionaisValidos(codigoDeBarra, codigoDeBarraFiltrado);

    }

    private static boolean digitosVerificadoresOpcionaisValidos(final int[] codigoDeBarra, final int[] codigoDeBarraFiltrado) {

        final int primeiroDigitoVerificador = CalculoModulo11
                .calcularDigitoVerificador(codigoDeBarraFiltrado, 8, 41 - DIGITOS_EXCLUIDOS);

        if (CalculoModulo11.digitoVerificadorIncorreto(INDICE_PRIMEIRO_DIGITO_VERIFICADOR, codigoDeBarra, primeiroDigitoVerificador))
            return false;

        final int segundoDigitoVerificador = CalculoModulo11
                .calcularDigitoVerificador(codigoDeBarraFiltrado, 41, 42 - DIGITOS_EXCLUIDOS);

        if (CalculoModulo11.digitoVerificadorIncorreto(INDICE_SEGUNDO_DIGITO_VERIFICADOR, codigoDeBarra, segundoDigitoVerificador))
            return false;

        final int terceiroDigitoVerificador = CalculoModulo11
                .calcularDigitoVerificador(codigoDeBarraFiltrado, 8, 43 - DIGITOS_EXCLUIDOS);

        return !CalculoModulo11.digitoVerificadorIncorreto(INDICE_TERCEIRO_DIGITO_VERIFICADOR, codigoDeBarra, terceiroDigitoVerificador);

    }

    private static int[] removerIndiceQuatroDoCodigoDeBarra(final int[] codigoDeBarra) {

        System
                .arraycopy(
                        codigoDeBarra,
                        INDICE_DIGITO_VERIFICADOR,
                        codigoDeBarra,
                        INDICE_DIGITO_VERIFICADOR - 1,
                        codigoDeBarra.length - INDICE_DIGITO_VERIFICADOR);

        return codigoDeBarra;

    }

    PredicadoCodigoDeBarra() {
    }

    @Override
    public boolean test(final String cadeiaNumericaCodigoDeBarra) {

        final int[] codigoDeBarra = CalculoModulo11.transformarCadeiaDeCaracteresEmUmArray(cadeiaNumericaCodigoDeBarra);

        return digitosVerificadoresValidos(codigoDeBarra);

    }

}
