package br.com.project.tax.payment.domain.contribuinte;

public class Contribuinte {

    public static ContribuinteConstrutor builder() {
        return new ContribuinteConstrutor();
    }

    public static class ContribuinteConstrutor {

        private ContribuinteConstrutor() {
        }

        private TipoNumeroContribuinte tipo;

        private String numeroContribuinte;

        public ContribuinteConstrutor comTipo(final int tipo) {
            this.tipo = TipoNumeroContribuinte.of(tipo);
            return this;
        }

        public ContribuinteConstrutor paraNumero(final String numeroContribuinte) {
            this.numeroContribuinte = numeroContribuinte;
            return this;
        }

        public Contribuinte build() {
            return new Contribuinte(this);
        }

    }

    public static Contribuinte mock() {

        return
                Contribuinte
                        .builder()
                        .comTipo(2)
                        .paraNumero("219.786.850-09")
                        .build();

    }

    private Contribuinte(final ContribuinteConstrutor construtor) {
        tipoNumeroContribuinte = construtor.tipo;
        numeroContribuinte = construtor.numeroContribuinte;
    }

    private final String numeroContribuinte;

    private final TipoNumeroContribuinte tipoNumeroContribuinte;

    public boolean valido() {
        return tipoNumeroContribuinte.predicado().test(numeroContribuinte);
    }

    public String getNumero() {
        return numeroContribuinte;
    }

    public int getCodigo() {
        return tipoNumeroContribuinte.getCodigo();
    }

}
