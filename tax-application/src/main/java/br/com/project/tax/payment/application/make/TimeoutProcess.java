package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
final class TimeoutProcess extends Process {

    TimeoutProcess(final StatusPayment statusPayment, final Process nextProcess, final Temporize temporize) {
        super(nextProcess, statusPayment);
        this.statusPayment = statusPayment;
        this.temporize = temporize;
    }

    private final StatusPayment statusPayment;

    private final Temporize temporize;

    @Override
    protected Optional<PaymentResponse> process(final PaymentRequest paymentRequest) {

        final String dataRequisicao = paymentRequest.obterDataRequisicao();

        final String horaRequisicao = paymentRequest.obterHoraRequisicao();

        final Timeout timeout =
                Timeout
                        .init
                                (
                                        Datas.paraDataHora
                                                (
                                                        dataRequisicao,
                                                        horaRequisicao
                                                ), temporize.init()
                                );

        if (timeout.exceeded()) return buildResponse(dataRequisicao, horaRequisicao);

        return Optional.empty();

    }

    private Optional<PaymentResponse> buildResponse(final String dataRequisicao, final String horaRequisicao) {

        return Optional
                .of(PaymentResponseBuilder
                        .builder()
                        .addError
                                (PaymentErrorBuilder
                                        .builder()
                                        .field(FieldError.HORA_REQUISICAO)
                                        .withMessageError(MessageError.TEMPO_EXCEDIDO)
                                        .value
                                                (Datas
                                                        .paraDataHora
                                                                (dataRequisicao, horaRequisicao)
                                                        .toString()
                                                )
                                        .build()
                                )
                        .withStatus(statusPayment.invalidRequest())
                        .build()
                );

    }

}
