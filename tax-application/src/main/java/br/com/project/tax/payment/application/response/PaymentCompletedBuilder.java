package br.com.project.tax.payment.application.response;

import java.util.HashSet;
import java.util.Set;

public class PaymentCompletedBuilder {

    public static PaymentCompletedBuilder builder() {
        return new PaymentCompletedBuilder();
    }

    private static class PaymentCompletedImpl implements PaymentCompleted {

        private PaymentCompletedImpl(final PaymentCompletedBuilder paymentCompletedBuilder) {
            protocolo = paymentCompletedBuilder.protocolo;
            payments = paymentCompletedBuilder.payments;
        }

        private final String protocolo;

        private final Set<Payment> payments;

        @Override
        public String protocolo() {
            return protocolo;
        }

        @Override
        public Set<Payment> payments() {
            return payments;
        }

    }

    private PaymentCompletedBuilder() {
        payments = new HashSet<>();
    }

    private String protocolo;

    private final Set<Payment> payments;

    public PaymentCompletedBuilder withProtocolo(final String protocolo) {
        this.protocolo = protocolo;
        return this;
    }

    public PaymentCompletedBuilder withPayments(final Set<Payment> payments) {
        this.payments.addAll(payments);
        return this;
    }

    public PaymentCompleted build() {
        return new PaymentCompletedImpl(this);
    }

}
