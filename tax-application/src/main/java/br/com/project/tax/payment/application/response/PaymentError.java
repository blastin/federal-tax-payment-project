package br.com.project.tax.payment.application.response;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface PaymentError {

    static PaymentError mock() {

        return PaymentErrorBuilder
                .builder()
                .field(FieldError.OUTRO)
                .withMessageError(MessageError.OUTRO)
                .value("null")
                .build();

    }

    String field();

    String value();

    int code();

    String description();

}
