package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.customer.CustomerQuery;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.time.Temporize;

public class UseCaseMakePaymentFactory {

    public static UseCaseMakePayment factory
            (
                    final StatusPayment statusPayment,
                    final DebitGateway debitQuery,
                    final CustomerQuery customerQuery,
                    final TaxRevenuesGateway taxRevenuesGateway,
                    final Temporize temporize
            ) {

        final Process taxProcess = taxFactory(statusPayment, taxRevenuesGateway, debitQuery, temporize);

        final Process debitSaveProcess =
                new DebitSaveProcess(taxProcess, statusPayment, debitQuery);

        final Process costumerExistenceProcess =
                new EnableCustomerProcess(debitSaveProcess, statusPayment, customerQuery);

        final Process paymentDuplicationProcess =
                new PaymentDuplicationProcess(costumerExistenceProcess, statusPayment, debitQuery);

        final Process timeoutProcess =
                new TimeoutProcess(statusPayment, paymentDuplicationProcess, temporize);

        return new RequestValidationProcess(timeoutProcess, statusPayment);

    }

    private static TaxProcess taxFactory
            (
                    final StatusPayment statusPayment,
                    final TaxRevenuesGateway taxRevenuesGateway,
                    final DebitCommand debitCommand,
                    final Temporize temporize
            ) {

        final TaxProcess paymentErrorProcess =
                new PaymentErrorProcess
                        (
                                null,
                                statusPayment,
                                taxRevenuesGateway,
                                null,
                                debitCommand,
                                temporize
                        );

        final TaxProcess rejectedPaymentProcess =
                new RejectedPaymentProcess
                        (
                                null,
                                statusPayment,
                                taxRevenuesGateway,
                                paymentErrorProcess,
                                debitCommand,
                                temporize
                        );

        final TaxProcess feePaidProcess =
                new FeePaidProcess
                        (
                                null,
                                statusPayment,
                                taxRevenuesGateway,
                                rejectedPaymentProcess,
                                debitCommand,
                                temporize
                        );

        return new ReversalTaxPayProcess
                (
                        null,
                        statusPayment,
                        taxRevenuesGateway,
                        feePaidProcess,
                        debitCommand,
                        temporize
                );

    }

    private UseCaseMakePaymentFactory() {
    }

}
