package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.EspecieDebito;

import java.util.Optional;

final class DebitSaveProcess extends Process {

    DebitSaveProcess
            (
                    final Process nextProcess,
                    final StatusPayment statusPayment,
                    final DebitCommand debitCommand
            ) {
        super(nextProcess, statusPayment);
        this.debitCommand = debitCommand;
    }

    private final DebitCommand debitCommand;

    @Override
    protected Optional<PaymentResponse> process(final PaymentRequest paymentRequest) {

        final Debit debit = Debit
                .builder()
                .comProtocolo(paymentRequest.obterProtocolo())
                .paraEspecie(EspecieDebito.of(paymentRequest.obterEspecieDebito()))
                .feitaPeloDespachante(paymentRequest.obterDespachante())
                .paraData(paymentRequest.obterDataRequisicao())
                .paraHora(paymentRequest.obterHoraRequisicao())
                .comReferencia(paymentRequest.obterReferenciaDebito())
                .comArrecadacoes(DocumentoArrecadacao.de(paymentRequest.obterCodigosDeBarra()))
                .build();

        debitCommand.save(debit);

        return Optional.empty();

    }

}
