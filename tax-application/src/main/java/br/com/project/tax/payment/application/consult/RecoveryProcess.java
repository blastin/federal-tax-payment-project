package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;

import java.util.Optional;

final class RecoveryProcess extends ConsultPaymentProcess {

    RecoveryProcess
            (
                    final ConsultPaymentProcess nextProcess,
                    final StatusPayment statusPayment,
                    final DebitQuery debitQuery,
                    final TaxRevenuesQuery taxRevenuesQuery
            ) {
        super(nextProcess, statusPayment);
        this.debitQuery = debitQuery;
        this.taxRevenuesQuery = taxRevenuesQuery;
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    private final DebitQuery debitQuery;

    private final TaxRevenuesQuery taxRevenuesQuery;

    @Override
    protected Optional<PaymentResponse> process(final String protocol) {

        final Optional<Debit> debitOptional = debitQuery.recoveryByProtocol(protocol);

        if (debitOptional.isEmpty()) return Optional.empty();

        final Debit debit = debitOptional.get();

        return Optional.of
                (
                        taxRevenuesQuery
                                .recoveryByProtocol(protocol)
                                .map(
                                        taxRevenues ->
                                                TaxRevenues
                                                        .builder(taxRevenues)
                                                        .withDebit(debit)
                                                        .build()
                                )
                                .map(
                                        taxRevenues ->
                                                PaymentResponseBuilder
                                                        .builder()
                                                        .withStatus(statusPayment.processRequest())
                                                        .completedWith
                                                                (
                                                                        PaymentCompletedBuilder
                                                                                .builder()
                                                                                .withProtocolo(protocol)
                                                                                .withPayments(PaymentBuilder.valueof(taxRevenues))
                                                                                .build()
                                                                )
                                                        .build()
                                )
                                .orElse
                                        (
                                                PaymentResponseBuilder
                                                        .builder()
                                                        .withStatus(statusPayment.invalidRequest())
                                                        .addError
                                                                (
                                                                        PaymentErrorBuilder
                                                                                .builder()
                                                                                .field(FieldError.SITUACAO)
                                                                                .withMessageError(MessageError.PAGAMENTO_NAO_EFETUADO)
                                                                                .value(debit.getDebitSituation().toString())
                                                                                .build()
                                                                )
                                                        .build()
                                        )
                );

    }

}
