package br.com.project.tax.payment.application.response;

import java.util.Optional;
import java.util.Set;

public final class PaymentResponseNull implements PaymentResponse {

    public PaymentResponseNull(final StatusPayment statusPayment) {
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    @Override
    public int status() {
        return statusPayment.errorProcess();
    }

    @Override
    public Set<PaymentError> errors() {
        return Set.of(PaymentError.mock());
    }

    @Override
    public Optional<PaymentCompleted> payment() {
        return Optional.empty();
    }

}
