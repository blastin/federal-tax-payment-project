package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface UseCaseMakePayment {

    static UseCaseMakePayment mock() {
        return paymentRequest -> PaymentResponse.mock();
    }

    PaymentResponse execute(final PaymentRequest paymentRequest);

}
