package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.PaymentResponse;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface UseCaseConsultPayment {

    static UseCaseConsultPayment mock() {
        return protocol -> PaymentResponse.mock();
    }

    PaymentResponse consult(final String protocol);

}
