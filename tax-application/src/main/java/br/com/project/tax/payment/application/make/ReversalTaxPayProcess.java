package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

final class ReversalTaxPayProcess extends TaxProcess {

    private final StatusPayment statusPayment;

    ReversalTaxPayProcess
            (
                    final Process nextProcess,
                    final StatusPayment statusPayment,
                    final TaxRevenuesTransaction taxRevenuesGateway,
                    final TaxProcess nextTaxProcess,
                    final DebitCommand debitCommand,
                    final Temporize temporize
            ) {
        super(nextProcess, statusPayment, taxRevenuesGateway, nextTaxProcess, temporize);
        this.debitCommand = debitCommand;
        this.taxRevenuesTransaction = taxRevenuesGateway;
        this.statusPayment = statusPayment;
    }

    private final DebitCommand debitCommand;

    private final TaxRevenuesTransaction taxRevenuesTransaction;

    @Override
    protected Optional<PaymentResponse> process
            (
                    final String protocol,
                    final AtomicReference<TaxRevenues> taxRevenues,
                    final TaxRevenuesSituation taxRevenuesSituation,
                    final Timeout timeout
            ) {

        final TaxRevenues revenues = taxRevenues.get();

        if (taxRevenuesSituation.equals(TaxRevenuesSituation.PAID) && timeout.exceeded()) {

            debitCommand.updateDebitSituation(protocol, DebitSituation.REVERSAL);

            taxRevenuesTransaction.reversal(revenues.getAutenticationNumber());

            return buildResponse(timeout);

        }

        return Optional.empty();

    }

    private Optional<PaymentResponse> buildResponse(final Timeout timeout) {

        return Optional
                .of
                        (
                                PaymentResponseBuilder
                                        .builder()
                                        .withStatus(statusPayment.invalidRequest())
                                        .addError
                                                (
                                                        PaymentErrorBuilder
                                                                .builder()
                                                                .value(timeout.toString())
                                                                .field(FieldError.HORA_REQUISICAO)
                                                                .withMessageError(MessageError.TEMPO_EXCEDIDO)
                                                                .build()
                                                )
                                        .build()
                        );

    }

}

