package br.com.project.tax.payment.application.validation;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
@FunctionalInterface
public interface Produce<T> {
    T get();
}
