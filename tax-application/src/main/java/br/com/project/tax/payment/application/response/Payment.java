package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.time.Datas;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface Payment {

    static Payment mock() {
        return new SkeletalPaymentMock();
    }

    class SkeletalPaymentMock implements Payment {

        private SkeletalPaymentMock() {
            taxRevenues = TaxRevenues.mock();
        }

        private final TaxRevenues taxRevenues;

        @Override
        public String codigoDeBarra() {
            return DocumentoArrecadacao.mock().getCodigoDeBarra();
        }

        @Override
        public String numeroAutenticacao() {
            return taxRevenues.getAutenticationNumber();
        }

        @Override
        public String dataArrecadacao() {
            return Datas.formatarData(taxRevenues.getTaxRevenuesDate());
        }

        @Override
        public String horaArrecadacao() {
            return Datas.formatarHora(taxRevenues.getTaxRevenuesTime());
        }

    }

    String codigoDeBarra();

    String numeroAutenticacao();

    String dataArrecadacao();

    String horaArrecadacao();

}
