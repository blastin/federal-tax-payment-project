package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.debito.Debit;

import java.util.Set;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface PaymentCompleted {

    static PaymentCompleted mock() {
        return PaymentCompletedBuilder
                .builder()
                .withProtocolo
                        (
                                Debit
                                        .mock()
                                        .getProtocol()
                        )
                .withPayments(Set.of(Payment.mock()))
                .build();
    }

    String protocolo();

    Set<Payment> payments();

}
