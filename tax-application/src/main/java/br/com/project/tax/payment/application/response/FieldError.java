package br.com.project.tax.payment.application.response;

public enum FieldError {

    PROTOCOL("protocolo"),
    BANCO("codigoBanco"),
    AGENCIA("agencia"),
    CONTA_CORRENTE("contaCorrente"),
    DESPACHANTE("despachante"),
    CONTRIBUINTE("Contribuinte"),
    ESPECIE_DEBITO("especieDebito"),
    REFERENCIA_DEBITO("referenciaDebito"),
    DATA_REQUISICAO("dataRequisicao"),
    HORA_REQUISICAO("horaRequisicao"),
    CODIGO_DE_BARRA("codigoBarra"),
    SITUACAO("situação de débito"),
    OUTRO("null");

    FieldError(String nome) {
        this.nome = nome;
    }

    private final String nome;

    @Override
    public String toString() {
        return nome;
    }

}
