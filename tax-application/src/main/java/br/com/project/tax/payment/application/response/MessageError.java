package br.com.project.tax.payment.application.response;

public enum MessageError {

    INVALIDO(1, "Campo invalido"),
    REJEITADO(3, "rejeitado"),
    PAGAMENTO_DUPLICADO(4, "Número do protocolo DARA já existente na base de dados"),
    CODIGO_DE_BARRA_LIMITE(6, "Quantidade de código de barra acima/abaixo do limite"),
    TEMPO_EXCEDIDO(8, "Tempo máximo excedido"),
    CLIENTE_NAO_HABILITADO(9, "Pagador não está habilitado a pagar taxa de atributo federal"),
    INEXISTENTE(10, "Número de prótocolo inexistente"),
    PAGAMENTO_NAO_EFETUADO(11, "Pagamento de taxa federal não foi efetuado"),
    OUTRO(99, "Operação não pode ser completada. Tente novamente");

    MessageError(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    private final String descricao;

    private final int codigo;

    public int toInt() {
        return codigo;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
