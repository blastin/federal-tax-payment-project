package br.com.project.tax.payment.application.response;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class PaymentResponseBuilder {

    public static PaymentResponseBuilder builder() {
        return new PaymentResponseBuilder();
    }

    private PaymentResponseBuilder() {
        paymentErrors = new HashSet<>();
    }

    private int status;

    private final Set<PaymentError> paymentErrors;

    private PaymentCompleted paymentCompleted;

    public PaymentResponseBuilder withStatus(final int status) {
        this.status = status;
        return this;
    }

    public PaymentResponseBuilder completedWith(final PaymentCompleted paymentCompleted) {
        paymentErrors.clear();
        this.paymentCompleted = paymentCompleted;
        return this;
    }

    public PaymentResponseBuilder addError(final PaymentError paymentError) {
        this.paymentCompleted = null;
        paymentErrors.add(paymentError);
        return this;
    }

    public PaymentResponseBuilder addAllErrors(final Set<PaymentError> paymentErrors) {
        this.paymentCompleted = null;
        this.paymentErrors.addAll(paymentErrors);
        return this;
    }

    public PaymentResponse build() {
        return new PaymentResponseImpl(this);
    }

    private static class PaymentResponseImpl implements PaymentResponse {

        private PaymentResponseImpl(final PaymentResponseBuilder paymentResponseBuilder) {
            status = paymentResponseBuilder.status;
            paymentErrors = paymentResponseBuilder.paymentErrors;
            paymentCompleted = paymentResponseBuilder.paymentCompleted;
        }

        private final int status;

        private final Set<PaymentError> paymentErrors;

        private final PaymentCompleted paymentCompleted;

        @Override
        public int status() {
            return status;
        }

        @Override
        public Set<PaymentError> errors() {
            return paymentErrors;
        }

        @Override
        public Optional<PaymentCompleted> payment() {
            return Optional.ofNullable(paymentCompleted);
        }

    }

}
