package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesCommand;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesSituation;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

final class FeePaidProcess extends TaxProcess {

    FeePaidProcess
            (
                    final Process nextProcess,
                    final StatusPayment statusPayment,
                    final TaxRevenuesGateway taxRevenuesGateway,
                    final TaxProcess nextTaxProcess,
                    final DebitCommand debitCommand,
                    final Temporize temporize
            ) {
        super(nextProcess, statusPayment, taxRevenuesGateway, nextTaxProcess, temporize);
        this.taxRevenuesCommand = taxRevenuesGateway;
        this.debitCommand = debitCommand;
        this.statusPayment = statusPayment;
    }

    private final TaxRevenuesCommand taxRevenuesCommand;
    private final DebitCommand debitCommand;
    private final StatusPayment statusPayment;

    @Override
    protected Optional<PaymentResponse> process
            (
                    final String protocol,
                    final AtomicReference<TaxRevenues> taxRevenues,
                    final TaxRevenuesSituation taxRevenuesSituation,
                    final Timeout timeout
            ) {

        if (taxRevenuesSituation.equals(TaxRevenuesSituation.PAID) && !timeout.exceeded()) {

            final TaxRevenues revenues = taxRevenues.get();

            taxRevenuesCommand.save(revenues);

            debitCommand.updateDebitSituation(protocol, DebitSituation.CONFIRMED);

            return buildResponse(protocol, revenues);

        }

        return Optional.empty();

    }

    private Optional<PaymentResponse> buildResponse(final String protocol, final TaxRevenues revenues) {

        return Optional.of
                (
                        PaymentResponseBuilder
                                .builder()
                                .withStatus(statusPayment.processRequest())
                                .completedWith
                                        (
                                                PaymentCompletedBuilder
                                                        .builder()
                                                        .withProtocolo(protocol)
                                                        .withPayments(PaymentBuilder.valueof(revenues))
                                                        .build()
                                        )
                                .build()
                );

    }
}
