package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

final class RejectedPaymentProcess extends TaxProcess {

    RejectedPaymentProcess
            (final Process nextProcess,
             final StatusPayment statusPayment,
             final TaxRevenuesTransaction taxRevenuesTransaction,
             final TaxProcess nextTaxProcess,
             final DebitCommand debitCommand,
             final Temporize temporize
            ) {
        super(nextProcess, statusPayment, taxRevenuesTransaction, nextTaxProcess, temporize);
        this.debitCommand = debitCommand;
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    private final DebitCommand debitCommand;

    @Override
    protected Optional<PaymentResponse> process
            (
                    final String protocol,
                    final AtomicReference<TaxRevenues> taxRevenues,
                    final TaxRevenuesSituation taxRevenuesSituation,
                    final Timeout timeout
            ) {

        if (taxRevenuesSituation.equals(TaxRevenuesSituation.NOT_PAID)) {

            debitCommand.updateDebitSituation(protocol, DebitSituation.INSUFFICIENT_FUNDS);

            return buildResponse(protocol);
        }

        return Optional.empty();

    }

    private Optional<PaymentResponse> buildResponse(final String protocol) {

        return Optional
                .of
                        (
                                PaymentResponseBuilder
                                        .builder()
                                        .withStatus(statusPayment.invalidRequest())
                                        .addError
                                                (
                                                        PaymentErrorBuilder
                                                                .builder()
                                                                .withMessageError(MessageError.REJEITADO)
                                                                .field(FieldError.PROTOCOL)
                                                                .value(protocol)
                                                                .build()
                                                )
                                        .build()
                        );

    }
}
