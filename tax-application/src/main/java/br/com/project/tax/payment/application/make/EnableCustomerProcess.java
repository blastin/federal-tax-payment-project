package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.customer.CustomerQuery;

import java.util.Optional;

final class EnableCustomerProcess extends Process {

    EnableCustomerProcess
            (
                    final Process nextProcess,
                    final StatusPayment statusPayment,
                    final CustomerQuery customerQuery
            ) {
        super(nextProcess, statusPayment);
        this.customerQuery = customerQuery;
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    private final CustomerQuery customerQuery;

    @Override
    protected Optional<PaymentResponse> process(final PaymentRequest paymentRequest) {

        final Customer customer =
                Customer
                        .builder()
                        .comValorContribuinte(paymentRequest.obterContribuinte())
                        .contemCodigoAgencia(paymentRequest.obterAgencia())
                        .paraBanco(paymentRequest.obterBanco())
                        .paraContaCorrente(paymentRequest.obterContaCorrente())
                        .build();

        if (customerQuery.existsCostumer(customer)) return Optional.empty();

        return buildResponse(customer);

    }

    private Optional<PaymentResponse> buildResponse(final Customer customer) {

        return Optional.of
                (PaymentResponseBuilder
                        .builder()
                        .withStatus(statusPayment.invalidRequest())
                        .addError
                                (
                                        PaymentErrorBuilder
                                                .builder()
                                                .value(customer.getContribuinte().getNumero())
                                                .withMessageError(MessageError.CLIENTE_NAO_HABILITADO)
                                                .field(FieldError.CONTRIBUINTE)
                                                .build()
                                )
                        .build());

    }

}
