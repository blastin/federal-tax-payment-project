package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.EspecieDebito;
import br.com.project.tax.payment.domain.tax.*;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Abstract class to do step of federal tax payment
 * <p>
 *
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
abstract class TaxProcess extends Process {

    private static TaxRevenuesPayment buildPayment(final PaymentRequest paymentRequest, final Timeout timeOut) {

        return TaxRevenuesPayment
                .builder()
                .withCodigosDeBarra(paymentRequest.obterCodigosDeBarra())
                .withTimeOut(timeOut)
                .toDebit
                        (
                                Debit
                                        .builder()
                                        .comProtocolo(paymentRequest.obterProtocolo())
                                        .paraEspecie(EspecieDebito.of(paymentRequest.obterEspecieDebito()))
                                        .feitaPeloDespachante(paymentRequest.obterDespachante())
                                        .paraData(paymentRequest.obterDataRequisicao())
                                        .paraHora(paymentRequest.obterHoraRequisicao())
                                        .comReferencia(paymentRequest.obterReferenciaDebito())
                                        .comArrecadacoes(DocumentoArrecadacao.de(paymentRequest.obterCodigosDeBarra()))
                                        .build()
                        )
                .withCustomer
                        (
                                Customer
                                        .builder()
                                        .comValorContribuinte(paymentRequest.obterContribuinte())
                                        .contemCodigoAgencia(paymentRequest.obterAgencia())
                                        .paraBanco(paymentRequest.obterBanco())
                                        .paraContaCorrente(paymentRequest.obterContaCorrente())
                                        .build()
                        )
                .build();

    }

    private static void buildTaxRevenuesIfPaid
            (
                    final TaxRevenuesPayment taxRevenuesPayment,
                    final AtomicReference<TaxRevenues> taxRevenuesAtomicReference,
                    final AtomicReference<TaxRevenuesSituation> taxTevenuesSituation,
                    final TaxRevenues taxRevenuesExist
            ) {

        taxRevenuesAtomicReference.set
                (
                        TaxRevenues
                                .builder(taxRevenuesExist)
                                .withPayment(taxRevenuesPayment)
                                .build()
                );

        taxTevenuesSituation.set(TaxRevenuesSituation.PAID);

    }

    protected TaxProcess
            (
                    final Process nextProcess,
                    final StatusPayment statusPayment,
                    final TaxRevenuesTransaction taxRevenuesTransaction,
                    final TaxProcess nextTaxProcess,
                    final Temporize temporize
            ) {
        super(nextProcess, statusPayment);
        this.taxRevenuesTransaction = taxRevenuesTransaction;
        this.nextTaxProcess = nextTaxProcess;
        this.temporize = temporize;
    }

    private final TaxProcess nextTaxProcess;

    private final TaxRevenuesTransaction taxRevenuesTransaction;

    private final Temporize temporize;

    @Override
    protected final Optional<PaymentResponse> process(final PaymentRequest paymentRequest) {

        final LocalDateTime dateTimeRequest = Datas
                .paraDataHora
                        (
                                paymentRequest.obterDataRequisicao(),
                                paymentRequest.obterHoraRequisicao()
                        );

        final Timeout timeOut = Timeout.init(dateTimeRequest, temporize.payment());

        final TaxRevenuesPayment taxRevenuesPayment = buildPayment(paymentRequest, timeOut);

        return payRevenues(taxRevenuesPayment, timeOut);

    }

    private Optional<PaymentResponse> payRevenues(final TaxRevenuesPayment taxRevenuesPayment, final Timeout timeout) {

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicReference<TaxRevenuesSituation> taxTevenuesSituation =
                new AtomicReference<>(TaxRevenuesSituation.NOT_PAID);

        try {

            taxRevenuesTransaction
                    .payFederalFee(taxRevenuesPayment)
                    .ifPresent
                            (
                                    taxRevenuesExist ->
                                            buildTaxRevenuesIfPaid
                                                    (
                                                            taxRevenuesPayment,
                                                            taxRevenuesAtomicReference,
                                                            taxTevenuesSituation,
                                                            taxRevenuesExist
                                                    )
                            );

        } catch (TaxRevenuesTransactionException e) {
            e.printStackTrace();
            taxTevenuesSituation.set(TaxRevenuesSituation.PAYMENT_ERROR);
        }

        return execute(taxRevenuesPayment.getProtocol(), taxRevenuesAtomicReference, taxTevenuesSituation.get(), timeout);

    }

    private Optional<PaymentResponse> execute
            (
                    final String protocol,
                    final AtomicReference<TaxRevenues> taxRevenues,
                    final TaxRevenuesSituation taxRevenuesSituation,
                    final Timeout timeout
            ) {

        final Optional<PaymentResponse> paymentResponse = process(protocol, taxRevenues, taxRevenuesSituation, timeout);

        if (paymentResponse.isEmpty() && hashNext())
            return nextTaxProcess.execute(protocol, taxRevenues, taxRevenuesSituation, timeout);

        return paymentResponse;

    }

    private boolean hashNext() {
        return nextTaxProcess != null;
    }

    protected abstract Optional<PaymentResponse> process
            (
                    final String protocol,
                    final AtomicReference<TaxRevenues> taxRevenues,
                    final TaxRevenuesSituation taxRevenuesSituation,
                    final Timeout timeout
            );

}

