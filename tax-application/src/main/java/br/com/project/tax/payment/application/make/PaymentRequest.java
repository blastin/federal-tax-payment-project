package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.domain.contribuinte.Contribuinte;
import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.EspecieDebito;

import java.util.List;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface PaymentRequest {

    static PaymentRequest mock() {
        return new SkeletalPaymentRequestMock();
    }

    class SkeletalPaymentRequestMock implements PaymentRequest {

        private static final Debit DEBIT_MOCK = Debit.mock();

        private static final Customer CUSTOMER_MOCK = Customer.mock();

        private static final DocumentoArrecadacao DOCUMENTO_ARRECADACAO = DocumentoArrecadacao.mock();

        @Override
        public String obterProtocolo() {
            return DEBIT_MOCK.getProtocol();
        }

        @Override
        public String obterBanco() {
            return CUSTOMER_MOCK.getCodigoBanco();
        }

        @Override
        public String obterAgencia() {
            return CUSTOMER_MOCK.getCodigoAgencia();
        }

        @Override
        public String obterContaCorrente() {
            return CUSTOMER_MOCK.getContaCorrente();
        }

        @Override
        public String obterDespachante() {
            return DEBIT_MOCK.getDespachante();
        }

        @Override
        public Contribuinte obterContribuinte() {
            return CUSTOMER_MOCK.getContribuinte();
        }

        @Override
        public int obterEspecieDebito() {
            return EspecieDebito.PUCOMEX.toInt();
        }

        @Override
        public String obterReferenciaDebito() {
            return DEBIT_MOCK.getReferenciaDebito();
        }

        @Override
        public String obterDataRequisicao() {
            return DEBIT_MOCK.getDataRequisicao();
        }

        @Override
        public String obterHoraRequisicao() {
            return DEBIT_MOCK.getHoraRequisicao();
        }

        @Override
        public List<String> obterCodigosDeBarra() {
            return List.of(DOCUMENTO_ARRECADACAO.getCodigoDeBarra());
        }

    }

    String obterProtocolo();

    String obterBanco();

    String obterAgencia();

    String obterContaCorrente();

    String obterDespachante();

    Contribuinte obterContribuinte();

    int obterEspecieDebito();

    String obterReferenciaDebito();

    String obterDataRequisicao();

    String obterHoraRequisicao();

    List<String> obterCodigosDeBarra();

}
