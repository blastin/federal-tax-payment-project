package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.DebitGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;

public class UseCaseConsultPaymentFactory {

    public static UseCaseConsultPayment factory
            (
                    final StatusPayment statusPayment,
                    final DebitGateway debitGateway,
                    final TaxRevenuesQuery taxRevenuesQuery
            ) {

        final ConsultPaymentProcess debitNotFoundProcess = new DebitNotFoundProcess(null, statusPayment);

        final ConsultPaymentProcess recoveryProcess =
                new RecoveryProcess(debitNotFoundProcess, statusPayment, debitGateway, taxRevenuesQuery);

        return new ValidateRequestProcess(recoveryProcess, statusPayment);

    }

    private UseCaseConsultPaymentFactory() {
    }

}
