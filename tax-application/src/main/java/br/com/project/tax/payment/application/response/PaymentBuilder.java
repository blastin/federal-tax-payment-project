package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.time.Datas;

import java.util.Set;
import java.util.stream.Collectors;

public class PaymentBuilder {

    public static Set<Payment> valueof(final TaxRevenues taxRevenues) {

        return taxRevenues
                .getCodigosDeBarra()
                .stream()
                .map(codigo -> new PaymentImpl(codigo, taxRevenues))
                .collect(Collectors.toUnmodifiableSet());

    }

    private static final class PaymentImpl implements Payment {

        private PaymentImpl(final String codigoDeBarra, final TaxRevenues taxRevenues) {
            this.codigoDeBarra = codigoDeBarra;
            this.taxRevenues = taxRevenues;
        }

        private final String codigoDeBarra;

        private final TaxRevenues taxRevenues;

        @Override
        public String codigoDeBarra() {
            return codigoDeBarra;
        }

        @Override
        public String numeroAutenticacao() {
            return taxRevenues.getAutenticationNumber();
        }

        @Override
        public String dataArrecadacao() {
            return Datas.formatarData(taxRevenues.getTaxRevenuesDate());
        }

        @Override
        public String horaArrecadacao() {
            return Datas.formatarHora(taxRevenues.getTaxRevenuesTime());
        }

    }

    private PaymentBuilder() {
    }

}
