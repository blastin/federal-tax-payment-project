package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.PaymentResponseNull;
import br.com.project.tax.payment.application.response.StatusPayment;

import java.util.Objects;
import java.util.Optional;

abstract class ConsultPaymentProcess implements UseCaseConsultPayment {

    private static class ConsultPaymentProcessNull extends ConsultPaymentProcess {

        private ConsultPaymentProcessNull(final StatusPayment statusPayment) {
            this.statusPayment = statusPayment;
        }

        private final StatusPayment statusPayment;

        @Override
        protected Optional<PaymentResponse> process(final String protocol) {
            return Optional.of(new PaymentResponseNull(statusPayment));
        }

    }

    protected ConsultPaymentProcess(final ConsultPaymentProcess nextProcess, final StatusPayment statusPayment) {
        this.nextProcess = nextProcess == null ? new ConsultPaymentProcessNull(statusPayment) : nextProcess;
    }

    private ConsultPaymentProcess() {
        nextProcess = null;
    }

    private final ConsultPaymentProcess nextProcess;

    @Override
    public PaymentResponse consult(final String protocol) {
        return process(protocol).orElseGet(() -> Objects.requireNonNull(nextProcess).consult(protocol));
    }

    protected abstract Optional<PaymentResponse> process(final String protocol);

}
