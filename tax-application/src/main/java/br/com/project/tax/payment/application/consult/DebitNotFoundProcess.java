package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.*;

import java.util.Optional;

final class DebitNotFoundProcess extends ConsultPaymentProcess {

    DebitNotFoundProcess(final ConsultPaymentProcess nextProcess, final StatusPayment statusPayment) {
        super(nextProcess, statusPayment);
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    @Override
    protected Optional<PaymentResponse> process(final String protocol) {
        return Optional.of
                (
                        PaymentResponseBuilder
                                .builder()
                                .withStatus(statusPayment.invalidRequest())
                                .addError
                                        (
                                                PaymentErrorBuilder.
                                                        builder()
                                                        .value(protocol)
                                                        .withMessageError(MessageError.INEXISTENTE)
                                                        .field(FieldError.PROTOCOL)
                                                        .build()
                                        )
                                .build()
                );
    }

}
