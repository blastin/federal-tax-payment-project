package br.com.project.tax.payment.application.validation;

import br.com.project.tax.payment.application.response.*;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public class RequestValidationAPI {

    public static RequestValidationAPI builder() {
        return new RequestValidationAPI();
    }

    private static <T> Predicate<T> not(final Predicate<T> predicate) {
        return t -> !predicate.test(t);
    }

    private RequestValidationAPI() {
        paymentErrors = new HashSet<>();
    }

    private final Set<PaymentError> paymentErrors;

    private void add(final PaymentError paymentError) {
        paymentErrors.add(paymentError);
    }

    public <T> RequestValidationAPI ifInvalid
            (
                    final Produce<T> producer,
                    final Predicate<T> predicate,
                    final FieldError fieldError,
                    final Produce<String> value,
                    final MessageError messageError
            ) {

        if (not(predicate).test(producer.get()))

            add
                    (
                            PaymentErrorBuilder
                                    .builder()
                                    .value(value.get())
                                    .field(fieldError)
                                    .withMessageError(messageError)
                                    .build()
                    );

        return this;

    }

    public <T> RequestValidationAPI ifInvalid
            (
                    final Produce<List<T>> producer,
                    final Predicate<T> predicate,
                    final Produce<String> value,
                    final FieldError fieldError,
                    final MessageError messageError
            ) {

        producer
                .get()
                .stream()
                .filter(not(predicate))
                .map(t -> PaymentErrorBuilder
                        .builder()
                        .value(value.get())
                        .field(fieldError)
                        .withMessageError(messageError)
                        .build())
                .forEach(this::add);

        return this;

    }

    public Optional<PaymentResponse> build(final StatusPayment statusPayment) {

        if (paymentErrors.isEmpty()) return Optional.empty();

        return Optional.of(PaymentResponseBuilder
                .builder()
                .withStatus(statusPayment.invalidRequest())
                .addAllErrors(paymentErrors)
                .build());

    }

}
