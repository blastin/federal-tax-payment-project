package br.com.project.tax.payment.application.response;

public class PaymentErrorBuilder {

    private static class PaymentErrorImpl implements PaymentError {

        private PaymentErrorImpl(final PaymentErrorBuilder paymentErrorBuilder) {
            messageError = paymentErrorBuilder.messageError;
            value = paymentErrorBuilder.value;
            field = paymentErrorBuilder.field;
        }

        private final MessageError messageError;

        private final String value;

        private final FieldError field;

        @Override
        public String field() {
            return field.toString();
        }

        @Override
        public String value() {
            return value;
        }

        @Override
        public int code() {
            return messageError.toInt();
        }

        @Override
        public String description() {
            return messageError.toString();
        }

    }

    public static PaymentErrorBuilder builder() {
        return new PaymentErrorBuilder();
    }

    private PaymentErrorBuilder() {
    }

    private MessageError messageError;

    private FieldError field;

    private String value;

    public PaymentErrorBuilder withMessageError(final MessageError messageError) {
        this.messageError = messageError;
        return this;
    }

    public PaymentErrorBuilder value(final String value) {
        this.value = value;
        return this;
    }

    public PaymentErrorBuilder field(final FieldError field) {
        this.field = field;
        return this;
    }

    public PaymentError build() {
        return new PaymentErrorImpl(this);
    }

}
