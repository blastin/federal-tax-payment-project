package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import br.com.project.tax.payment.domain.debito.DebitQuery;

import java.util.Optional;

final class PaymentDuplicationProcess extends Process {

    PaymentDuplicationProcess(final Process nextProcess, final StatusPayment statusPayment, final DebitQuery debitQuery) {
        super(nextProcess, statusPayment);
        this.debitQuery = debitQuery;
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    private final DebitQuery debitQuery;

    @Override
    protected Optional<PaymentResponse> process(final PaymentRequest paymentRequest) {

        final String protocol = paymentRequest.obterProtocolo();

        if (debitQuery.existsDebitByProtocol(protocol)) return buildResponse(protocol);

        return Optional.empty();

    }

    private Optional<PaymentResponse> buildResponse(final String protocol) {

        return Optional.of
                (
                        PaymentResponseBuilder
                                .builder()
                                .withStatus(statusPayment.invalidRequest())
                                .addError(
                                        PaymentErrorBuilder
                                                .builder()
                                                .field(FieldError.PROTOCOL)
                                                .withMessageError(MessageError.PAGAMENTO_DUPLICADO)
                                                .value(protocol)
                                                .build()
                                ).build()
                );

    }

}
