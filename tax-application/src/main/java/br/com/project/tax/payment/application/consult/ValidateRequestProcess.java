package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.application.validation.RequestValidationAPI;
import br.com.project.tax.payment.domain.debito.Debit;

import java.util.Optional;

final class ValidateRequestProcess extends ConsultPaymentProcess {

    ValidateRequestProcess(final ConsultPaymentProcess nextProcess, final StatusPayment statusPayment) {
        super(nextProcess, statusPayment);
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    @Override
    protected Optional<PaymentResponse> process(final String protocol) {

        return RequestValidationAPI
                .builder()
                .ifInvalid
                        (
                                () -> protocol,
                                Debit::protocoloValido,
                                FieldError.PROTOCOL,
                                () -> protocol,
                                MessageError.INVALIDO
                        )
                .build(statusPayment);

    }

}
