package br.com.project.tax.payment.application.response;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface StatusPayment {

    static StatusPayment mock() {
        return new SkeletalStatusPaymentMock();
    }

    class SkeletalStatusPaymentMock implements StatusPayment {

        private SkeletalStatusPaymentMock() {
            this.status = 0;
        }

        private final int status;

        @Override
        public int processRequest() {
            return status;
        }

        @Override
        public int invalidRequest() {
            return status + 1;
        }

        @Override
        public int errorProcess() {
            return status + 2;
        }

    }

    int processRequest();

    int invalidRequest();

    int errorProcess();

}
