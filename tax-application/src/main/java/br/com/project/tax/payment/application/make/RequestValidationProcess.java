package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.application.validation.RequestValidationAPI;
import br.com.project.tax.payment.domain.contribuinte.Contribuinte;
import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.EspecieDebito;

import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
final class RequestValidationProcess extends Process {

    RequestValidationProcess(final Process nextProcess, final StatusPayment statusPayment) {
        super(nextProcess, statusPayment);
        this.statusPayment = statusPayment;
    }

    private final StatusPayment statusPayment;

    @Override
    protected Optional<PaymentResponse> process(final PaymentRequest r) {

        return RequestValidationAPI
                .builder()
                .ifInvalid
                        (
                                r::obterCodigosDeBarra,
                                DocumentoArrecadacao::documentoValido,
                                r.obterCodigosDeBarra().iterator()::next,
                                FieldError.CODIGO_DE_BARRA,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterCodigosDeBarra,
                                DocumentoArrecadacao::quantidadeDocumentoValido,
                                FieldError.CODIGO_DE_BARRA,
                                FieldError.CODIGO_DE_BARRA::toString,
                                MessageError.CODIGO_DE_BARRA_LIMITE
                        )
                .ifInvalid
                        (
                                r::obterAgencia,
                                Customer::agenciaValida,
                                FieldError.AGENCIA,
                                r::obterAgencia,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterBanco,
                                Customer::bancoValido,
                                FieldError.BANCO,
                                r::obterBanco,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterContaCorrente,
                                Customer::contaCorrenteValida,
                                FieldError.CONTA_CORRENTE,
                                r::obterContaCorrente,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterContribuinte,
                                Contribuinte::valido,
                                FieldError.CONTRIBUINTE,
                                r.obterContribuinte()::getNumero,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterEspecieDebito,
                                Debit::especieDebitoValida,
                                FieldError.ESPECIE_DEBITO,
                                EspecieDebito.of(r.obterEspecieDebito())::toString,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterDataRequisicao,
                                Debit::dataRequisicaoValida,
                                FieldError.DATA_REQUISICAO,
                                r::obterDataRequisicao,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterDespachante,
                                Debit::despachanteValido,
                                FieldError.DESPACHANTE,
                                r::obterDespachante,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterHoraRequisicao,
                                Debit::horaRequisicaoValida,
                                FieldError.HORA_REQUISICAO,
                                r::obterHoraRequisicao,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterProtocolo,
                                Debit::protocoloValido,
                                FieldError.PROTOCOL,
                                r::obterProtocolo,
                                MessageError.INVALIDO
                        )
                .ifInvalid
                        (
                                r::obterReferenciaDebito,
                                Debit::referenciaDebitoValida,
                                FieldError.REFERENCIA_DEBITO,
                                r::obterReferenciaDebito,
                                MessageError.INVALIDO
                        )
                .build(statusPayment);

    }

}
