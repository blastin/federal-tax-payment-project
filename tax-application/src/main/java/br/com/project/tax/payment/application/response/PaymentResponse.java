package br.com.project.tax.payment.application.response;

import java.util.Optional;
import java.util.Set;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
public interface PaymentResponse {

    int status();

    Set<PaymentError> errors();

    Optional<PaymentCompleted> payment();

    static PaymentResponse mockError() {

        return PaymentResponseBuilder
                .builder()
                .addError(PaymentError.mock())
                .withStatus(StatusPayment.mock().errorProcess())
                .build();

    }

    static PaymentResponse mock() {

        return PaymentResponseBuilder
                .builder()
                .completedWith(PaymentCompleted.mock())
                .withStatus(0)
                .build();

    }
}
