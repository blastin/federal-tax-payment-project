package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.PaymentResponseNull;
import br.com.project.tax.payment.application.response.StatusPayment;

import java.util.Objects;
import java.util.Optional;

/**
 * @author Jefferson Lisboa (lisboa.jeff@gmail.com)
 */
abstract class Process implements UseCaseMakePayment {

    static final class ProcessNull extends Process {

        ProcessNull(final StatusPayment statusPayment) {
            paymentNull = new PaymentResponseNull(statusPayment);
        }

        private final PaymentResponseNull paymentNull;

        @Override
        protected Optional<PaymentResponse> process(PaymentRequest paymentRequest) {
            return Optional.of(paymentNull);
        }

    }

    public static Process mock() {
        return new ProcessNull(StatusPayment.mock());
    }

    protected Process(final Process nextProcess, final StatusPayment statusPayment) {
        this.nextProcess = nextProcess == null ? new ProcessNull(statusPayment) : nextProcess;
    }

    private Process() {
        this.nextProcess = null;
    }

    private final Process nextProcess;

    @Override
    public final PaymentResponse execute(final PaymentRequest paymentRequest) {
        return process(paymentRequest).orElseGet(() -> Objects.requireNonNull(nextProcess).execute(paymentRequest));
    }

    protected abstract Optional<PaymentResponse> process(final PaymentRequest paymentRequest);

}
