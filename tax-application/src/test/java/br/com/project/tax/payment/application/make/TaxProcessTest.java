package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.tax.*;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.Timeout;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

class TaxProcessTest {

    public static final Temporize TEMPORIZE = Temporize.mock();

    @Test
    void shouldTaxPayAndReturnResponseMock() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final PaymentResponse paymentResponseMock = PaymentResponse.mock();

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicBoolean situationAtomic = new AtomicBoolean(false);

        final TaxRevenues taxRevenues = TaxRevenues.mock();

        final TaxProcess taxProcess =
                new TaxProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.of(taxRevenues);
                                    }
                                },
                                null, TEMPORIZE
                        ) {
                    @Override
                    protected Optional<PaymentResponse> process
                            (
                                    final String protocol,
                                    final AtomicReference<TaxRevenues> taxRevenues,
                                    final TaxRevenuesSituation taxTevenuesSituation,
                                    final Timeout timeout
                            ) {
                        taxRevenuesAtomicReference.set(taxRevenues.get());
                        situationAtomic.set(taxTevenuesSituation.equals(TaxRevenuesSituation.PAID));
                        return Optional.of(paymentResponseMock);
                    }
                };

        final PaymentResponse paymentResponse = taxProcess.execute(PaymentRequest.mock());

        AssertPayment.completed(paymentResponse, PaymentRequest.mock());

        Assertions.assertTrue(situationAtomic.get());

        Assertions
                .assertNotNull(taxRevenuesAtomicReference.get());

        Assertions
                .assertEquals(taxRevenues, taxRevenuesAtomicReference.get());
    }

    @Test
    void shouldReturnResponseNullButWithNextProcessNotEqualsNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicBoolean situationAtomic = new AtomicBoolean(false);

        final TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction taxRevenuesTransaction =
                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                    @Override
                    public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment) {
                        return Optional.empty();
                    }
                };

        final Process taxProcess =
                new TaxProcess
                        (
                                null,
                                statusPayment,
                                taxRevenuesTransaction,
                                new TaxProcess(null, statusPayment, taxRevenuesTransaction, null, TEMPORIZE) {
                                    @Override
                                    protected Optional<PaymentResponse> process
                                            (
                                                    final String protocol,
                                                    final AtomicReference<TaxRevenues> taxRevenues,
                                                    final TaxRevenuesSituation taxTevenuesSituation,
                                                    final Timeout timeout
                                            ) {
                                        taxRevenuesAtomicReference.set(taxRevenues.get());
                                        situationAtomic.set(taxTevenuesSituation.equals(TaxRevenuesSituation.NOT_PAID));
                                        return Optional.of(PaymentResponse.mockError());
                                    }
                                }, TEMPORIZE
                        ) {
                    @Override
                    protected Optional<PaymentResponse> process
                            (
                                    final String protocol,
                                    final AtomicReference<TaxRevenues> taxRevenues,
                                    final TaxRevenuesSituation taxTevenuesSituation,
                                    final Timeout timeout
                            ) {
                        return Optional.empty();
                    }
                };

        final PaymentResponse paymentResponse = taxProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

        Assertions
                .assertTrue(situationAtomic.get());

        Assertions
                .assertNull(taxRevenuesAtomicReference.get());

    }

    @Test
    void shouldReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicBoolean situationAtomic = new AtomicBoolean(false);

        final Process taxProcess =
                new TaxProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.empty();
                                    }
                                },
                                null, TEMPORIZE
                        ) {
                    @Override
                    protected Optional<PaymentResponse> process
                            (
                                    final String protocol,
                                    final AtomicReference<TaxRevenues> taxRevenues,
                                    final TaxRevenuesSituation taxTevenuesSituation,
                                    final Timeout timeout
                            ) {
                        taxRevenuesAtomicReference.set(taxRevenues.get());
                        situationAtomic.set(taxTevenuesSituation.equals(TaxRevenuesSituation.NOT_PAID));
                        return Optional.empty();
                    }
                };

        final PaymentResponse paymentResponse = taxProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

        Assertions
                .assertTrue(situationAtomic.get());

        Assertions
                .assertNull(taxRevenuesAtomicReference.get());

    }

    @Test
    void shouldThrowTaxRevenuesTransactionExceptionAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicBoolean situationAtomic = new AtomicBoolean(false);

        final Process taxProcess =
                new TaxProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment)
                                            throws TaxRevenuesTransactionException {
                                        throw new TaxRevenuesTransactionException("transaction error");
                                    }
                                },
                                null, TEMPORIZE
                        ) {
                    @Override
                    protected Optional<PaymentResponse> process
                            (
                                    final String protocol,
                                    final AtomicReference<TaxRevenues> taxRevenues,
                                    final TaxRevenuesSituation taxTevenuesSituation,
                                    final Timeout timeout
                            ) {
                        taxRevenuesAtomicReference.set(taxRevenues.get());
                        situationAtomic.set(taxTevenuesSituation.equals(TaxRevenuesSituation.PAYMENT_ERROR));
                        return Optional.empty();
                    }
                };

        final PaymentResponse paymentResponse = taxProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

        Assertions
                .assertTrue(situationAtomic.get());

        Assertions
                .assertNull(taxRevenuesAtomicReference.get());

    }

    @Test
    void shouldTaxPayAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicReference<TaxRevenues> taxRevenuesAtomicReference = new AtomicReference<>();

        final AtomicBoolean situationAtomic = new AtomicBoolean(false);

        final TaxRevenues taxRevenues = TaxRevenues.mock();

        final Process taxProcess =
                new TaxProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.of(taxRevenues);
                                    }
                                },
                                null, TEMPORIZE
                        ) {
                    @Override
                    protected Optional<PaymentResponse> process
                            (
                                    final String protocol,
                                    final AtomicReference<TaxRevenues> taxRevenues,
                                    final TaxRevenuesSituation taxTevenuesSituation,
                                    final Timeout timeout
                            ) {
                        taxRevenuesAtomicReference.set(taxRevenues.get());
                        situationAtomic.set(taxTevenuesSituation.equals(TaxRevenuesSituation.PAID));
                        return Optional.empty();
                    }
                };

        final PaymentResponse paymentResponse = taxProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

        Assertions
                .assertTrue(situationAtomic.get());

        Assertions
                .assertNotNull(taxRevenuesAtomicReference.get());

        Assertions
                .assertEquals(taxRevenues, taxRevenuesAtomicReference.get());

    }

}
