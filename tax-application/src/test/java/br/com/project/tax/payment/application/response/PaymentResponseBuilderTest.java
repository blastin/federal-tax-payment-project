package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.make.PaymentRequest;
import org.junit.jupiter.api.Test;

class PaymentResponseBuilderTest {

    @Test
    void shouldBuildPaymentResponseToErrorResponse() {

        final PaymentResponse paymentResponse = PaymentResponse.mockError();

        AssertPayment.errorNull(StatusPayment.mock(), paymentResponse);

    }

    @Test
    void shouldBuildPaymentResponseToCompletedPayment() {

        final PaymentResponse paymentResponse = PaymentResponse.mock();

        AssertPayment.completed(paymentResponse, PaymentRequest.mock());

    }
}