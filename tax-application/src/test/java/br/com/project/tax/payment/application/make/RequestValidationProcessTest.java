package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.contribuinte.Contribuinte;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.EspecieDebito;
import org.junit.jupiter.api.Test;

import java.util.List;

class RequestValidationProcessTest {

    @Test
    void shouldReturnPaymentErrorForAgencia() {

        final String agencia = "47hruasdhsduhdsauh4uh8a83";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterAgencia() {
                return agencia;
            }

        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.AGENCIA, statusPayment.invalidRequest(), paymentResponse, agencia);

    }

    @Test
    void shouldReturnPaymentErrorForBanco() {

        final String banco = "47hruasdhsduhdsauh4uh8a83";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterBanco() {
                return banco;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.BANCO, statusPayment.invalidRequest(), paymentResponse, banco);

    }

    @Test
    void shouldReturnPaymentErrorForContaCorrente() {

        final String contaCorrente = "47hruasdhsduhdsauh4uh8a83";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterContaCorrente() {
                return contaCorrente;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.CONTA_CORRENTE, statusPayment.invalidRequest(), paymentResponse, contaCorrente);

    }

    @Test
    void shouldReturnPaymentErrorForContribuinte() {

        final String numeroContribuinte = "3048455_#4963";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public Contribuinte obterContribuinte() {
                return
                        Contribuinte
                                .builder()
                                .paraNumero(numeroContribuinte)
                                .comTipo(0)
                                .build();
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.CONTRIBUINTE, statusPayment.invalidRequest(), paymentResponse, numeroContribuinte);

    }

    @Test
    void shouldReturnPaymentErrorForEspecieDebito() {

        final EspecieDebito especieDebito = EspecieDebito.OUTRO;

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public int obterEspecieDebito() {
                return especieDebito.toInt();
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.ESPECIE_DEBITO, statusPayment.invalidRequest(), paymentResponse, especieDebito.toString());

    }

    @Test
    void shouldReturnPaymentErrorForDataRequisicao() {

        final String dataRequisicao = "03/04/10283";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterDataRequisicao() {
                return dataRequisicao;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.DATA_REQUISICAO, statusPayment.invalidRequest(), paymentResponse, dataRequisicao);

    }

    @Test
    void shouldReturnPaymentErrorForDespachante() {

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterDespachante() {
                return null;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.DESPACHANTE, statusPayment.invalidRequest(), paymentResponse, null);

    }

    @Test
    void shouldReturnPaymentErrorForHoraRequisicao() {

        final String horaRequisicao = "00?00:00";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterHoraRequisicao() {
                return horaRequisicao;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.HORA_REQUISICAO, statusPayment.invalidRequest(), paymentResponse, horaRequisicao);

    }

    @Test
    void shouldReturnPaymentErrorForProtocol() {

        final String protocol = "-10238547sh345";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterProtocolo() {
                return protocol;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.error(MessageError.INVALIDO, FieldError.PROTOCOL, statusPayment.invalidRequest(), paymentResponse, protocol);

    }

    @Test
    void shouldReturnPaymentErrorForDebitReference() {

        final String debitReference = "always";

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterReferenciaDebito() {
                return debitReference;
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.INVALIDO,
                                FieldError.REFERENCIA_DEBITO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                debitReference
                        );

    }

    @Test
    void shouldReturnPaymentErrorForDocumentoArrecadacaoLista() {

        final String codigoDeBarra = DocumentoArrecadacao.mock().getCodigoDeBarra().concat("234");

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public List<String> obterCodigosDeBarra() {
                return List.of(codigoDeBarra);
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.INVALIDO,
                                FieldError.CODIGO_DE_BARRA,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                codigoDeBarra
                        );

    }

    @Test
    void shouldReturnPaymentErrorForDocumentoArrecadacaoListaBecauseAmountIfLessThanOne() {

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {
            @Override
            public List<String> obterCodigosDeBarra() {
                return List.of();
            }
        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.CODIGO_DE_BARRA_LIMITE,
                                FieldError.CODIGO_DE_BARRA,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                FieldError.CODIGO_DE_BARRA.toString()
                        );

    }

    @Test
    void shouldReturnPaymentErrorNullObject() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process =
                new RequestValidationProcess(null, statusPayment);

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

}