package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.customer.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

class EnableCustomerProcessTest {

    @Test
    void shouldReturnCostumerDoesNotExist() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final EnableCustomerProcess makeThePaymentEnableCustomerProcess =
                new EnableCustomerProcess(null, statusPayment, costumer -> false);

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = makeThePaymentEnableCustomerProcess.execute(paymentRequest);

        AssertPayment.error
                (
                        MessageError.CLIENTE_NAO_HABILITADO,
                        FieldError.CONTRIBUINTE,
                        statusPayment.invalidRequest(),
                        paymentResponse,
                        paymentRequest.obterContribuinte().getNumero()
                );

    }

    @Test
    void shouldReturnCostumerExist() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicReference<Customer> costumerAtomicReference = new AtomicReference<>();

        final EnableCustomerProcess makeThePaymentEnableCustomerProcess =
                new EnableCustomerProcess(null, statusPayment, costumer -> {
                    costumerAtomicReference.set(costumer);
                    return true;
                });

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = makeThePaymentEnableCustomerProcess.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

        final Customer customer = costumerAtomicReference.get();

        Assertions
                .assertNotNull(customer);

        Assertions
                .assertEquals(paymentRequest.obterAgencia(), customer.getCodigoAgencia());

        Assertions
                .assertEquals(paymentRequest.obterBanco(), customer.getCodigoBanco());


        Assertions
                .assertEquals(paymentRequest.obterContaCorrente(), customer.getContaCorrente());


        Assertions
                .assertEquals(paymentRequest.obterContribuinte(), customer.getContribuinte());

    }

}