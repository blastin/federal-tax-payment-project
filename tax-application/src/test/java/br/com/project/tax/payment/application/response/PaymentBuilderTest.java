package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class PaymentBuilderTest {

    @Test
    void shouldBuildAPayment() {

        final Payment paymentMock = Payment.mock();

        final TaxRevenues taxRevenues = TaxRevenues.mock();

        final Set<Payment> payments = PaymentBuilder.valueof(taxRevenues);

        Assertions
                .assertNotNull(payments);

        Assertions
                .assertFalse(payments.isEmpty());

        Assertions
                .assertEquals(1, payments.size());

        final Payment payment = payments.iterator().next();

        Assertions
                .assertEquals(paymentMock.numeroAutenticacao(), payment.numeroAutenticacao());

        Assertions
                .assertEquals(paymentMock.codigoDeBarra(), payment.codigoDeBarra());

        Assertions
                .assertEquals(paymentMock.dataArrecadacao(), payment.dataArrecadacao());

        Assertions
                .assertEquals(paymentMock.horaArrecadacao(), payment.horaArrecadacao());

    }
}