package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.*;
import org.junit.jupiter.api.Assertions;

import java.util.Optional;
import java.util.Set;

public class AssertPayment {

    public static void completed(final PaymentResponse paymentResponse) {
        completed(paymentResponse, PaymentRequest.mock());
    }

    public static void completed(final PaymentResponse paymentResponse, final PaymentRequest paymentRequest) {

        Assertions
                .assertEquals(0, paymentResponse.status());

        Assertions
                .assertTrue(paymentResponse.errors().isEmpty());

        final Optional<PaymentCompleted> optionalPaymentCompletedCompleted = paymentResponse.payment();

        Assertions
                .assertTrue(optionalPaymentCompletedCompleted.isPresent());

        final PaymentCompleted paymentCompleted = optionalPaymentCompletedCompleted.get();

        Assertions
                .assertEquals(paymentRequest.obterProtocolo(), paymentCompleted.protocolo());

        final Set<Payment> payments = paymentCompleted.payments();

        Assertions
                .assertNotNull(payments);

        Assertions
                .assertFalse(payments.isEmpty());

        Assertions
                .assertEquals(1, payments.size());

        final Payment payment = payments.iterator().next();

        Assertions
                .assertEquals(paymentRequest.obterCodigosDeBarra().get(0), payment.codigoDeBarra());

        Assertions.assertNotNull(payment.horaArrecadacao());

        Assertions.assertNotNull(payment.dataArrecadacao());

        final Payment paymentMock = Payment.mock();

        Assertions
                .assertEquals(paymentMock.numeroAutenticacao(), payment.numeroAutenticacao());

    }

    public static void errorNull(final StatusPayment statusPayment, final PaymentResponse paymentResponse) {

        AssertPayment.error
                (
                        MessageError.OUTRO,
                        FieldError.OUTRO,
                        statusPayment.errorProcess(),
                        paymentResponse,
                        "null"
                );

    }

    public static void error
            (
                    final MessageError messageError,
                    final FieldError fieldError,
                    final int status,
                    final PaymentResponse paymentResponse,
                    final String value
            ) {

        Assertions
                .assertEquals(status, paymentResponse.status());

        Assertions
                .assertFalse(paymentResponse.errors().isEmpty());

        Assertions
                .assertTrue(paymentResponse.payment().isEmpty());

        Assertions
                .assertEquals(1, paymentResponse.errors().size());

        final PaymentError paymentError = paymentResponse.errors().iterator().next();

        Assertions
                .assertEquals(messageError.toString(), paymentError.description());

        Assertions
                .assertEquals(messageError.toInt(), paymentError.code());

        Assertions
                .assertEquals(fieldError.toString(), paymentError.field());

        Assertions
                .assertEquals(value, paymentError.value());

    }

    private AssertPayment() {
    }

}
