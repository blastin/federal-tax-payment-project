package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.TemporizeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

class FeePaidProcessTest {

    @Test
    void shouldBePaidWithoutTimeOverAndReturnResponsePaymentComplete() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final AtomicBoolean saved = new AtomicBoolean(false);

        final AtomicBoolean debitSituationAltered = new AtomicBoolean(false);

        final Process feePaidProcess =
                new FeePaidProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                    @Override
                                    public void save(TaxRevenues taxRevenues) {
                                        saved.set(true);
                                    }

                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.of(TaxRevenues.mock());
                                    }
                                },
                                null,
                                new DebitCommand.SkeletalDebitCommand() {
                                    @Override
                                    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                        debitSituationAltered.set(debitSituation.equals(DebitSituation.CONFIRMED));
                                    }
                                },
                                TemporizeBuilder.FIVE
                        );

        final PaymentResponse paymentResponse = feePaidProcess.execute(paymentRequest);

        AssertPayment
                .completed(paymentResponse, paymentRequest);

        Assertions
                .assertTrue(saved.get());

        Assertions
                .assertTrue(debitSituationAltered.get());
    }

    @Test
    void shouldBePaidWithTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final AtomicBoolean saved = new AtomicBoolean(false);

        final AtomicBoolean debitSituationAltered = new AtomicBoolean(false);

        final Process feePaidProcess =
                new FeePaidProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                    @Override
                                    public void save(TaxRevenues taxRevenues) {
                                        saved.set(true);
                                    }

                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.of(TaxRevenues.mock());
                                    }
                                },
                                null,
                                new DebitCommand.SkeletalDebitCommand() {
                                    @Override
                                    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                        debitSituationAltered.set(debitSituation.equals(DebitSituation.CONFIRMED));
                                    }
                                },
                                Temporize.mock()
                        );

        final PaymentResponse paymentResponse = feePaidProcess.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

        Assertions
                .assertFalse(saved.get());

        Assertions
                .assertFalse(debitSituationAltered.get());

    }

    @Test
    void shouldBeNotPaidWitTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final Process feePaidProcess =
                new FeePaidProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                    @Override
                                    public void save(TaxRevenues taxRevenues) {
                                        throw new RuntimeException("should'n here!");
                                    }

                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.empty();
                                    }
                                },
                                null,
                                new DebitCommand.SkeletalDebitCommand() {
                                    @Override
                                    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                        throw new RuntimeException("should'n here!");
                                    }
                                },
                                Temporize.mock()
                        );

        final PaymentResponse paymentResponse = feePaidProcess.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldBeNotPaidWithoutTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final Process feePaidProcess =
                new FeePaidProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                    @Override
                                    public void save(TaxRevenues taxRevenues) {
                                        throw new RuntimeException("should'n here!");
                                    }

                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.empty();
                                    }
                                },
                                null,
                                new DebitCommand.SkeletalDebitCommand() {
                                    @Override
                                    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                        throw new RuntimeException("should'n here!");
                                    }
                                },
                                TemporizeBuilder.FIVE
                        );

        final PaymentResponse paymentResponse = feePaidProcess.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

}