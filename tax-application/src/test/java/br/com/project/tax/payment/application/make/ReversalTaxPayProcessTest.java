package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.TemporizeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

class ReversalTaxPayProcessTest {

    @Test
    void shouldBePaidButWithTimeOverReturnResponse() {

        final AtomicBoolean reversed = new AtomicBoolean(false);

        final AtomicBoolean debitSituationAltered = new AtomicBoolean(false);

        final StatusPayment statusPayment = StatusPayment.mock();

        final LocalDateTime now = LocalDateTime.now();

        final String dataRequisicao = Datas.formatarData(now.toLocalDate());

        final String horaRequisicao = Datas.formatarHora(now.minusSeconds(14).toLocalTime());

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterDataRequisicao() {
                return dataRequisicao;
            }

            @Override
            public String obterHoraRequisicao() {
                return horaRequisicao;
            }

        };

        final Process payTaxWithTimeOverProcess =
                new ReversalTaxPayProcess
                        (
                                null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public void reversal(final String autenticationNumber) {
                                        reversed.set(true);
                                    }

                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.of(TaxRevenues.mock());
                                    }
                                },
                                null,
                                new DebitCommand.SkeletalDebitCommand() {
                                    @Override
                                    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                        debitSituationAltered.set(debitSituation.equals(DebitSituation.REVERSAL));
                                    }
                                },
                                Temporize.mock()
                        );

        final PaymentResponse paymentResponse = payTaxWithTimeOverProcess.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.TEMPO_EXCEDIDO,
                                FieldError.HORA_REQUISICAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                Datas
                                        .paraDataHora
                                                (
                                                        paymentRequest.obterDataRequisicao(),
                                                        paymentRequest.obterHoraRequisicao()
                                                ).toString()
                        );

        Assertions
                .assertTrue(reversed.get());

        Assertions
                .assertTrue(debitSituationAltered.get());

    }

    @Test
    void shouldBePaidButWithoutTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process payTaxWithTimeOverProcess =
                new ReversalTaxPayProcess
                        (null,
                                statusPayment,
                                TaxRevenuesTransaction.mock(),
                                null,
                                null,
                                TemporizeBuilder.FIVE
                        );

        final PaymentResponse paymentResponse = payTaxWithTimeOverProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldBeNotPaidButWithTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process payTaxWithTimeOverProcess =
                new ReversalTaxPayProcess
                        (null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.empty();
                                    }
                                },
                                null,
                                null,
                                Temporize.mock()
                        );

        final PaymentResponse paymentResponse = payTaxWithTimeOverProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldBeNotPaidWithoutTimeOverAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process payTaxWithTimeOverProcess =
                new ReversalTaxPayProcess
                        (null,
                                statusPayment,
                                new TaxRevenuesTransaction.SkeletalTaxRevenuesTransaction() {
                                    @Override
                                    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                        return Optional.empty();
                                    }
                                },
                                null,
                                null,
                                TemporizeBuilder.FIVE
                        );

        final PaymentResponse paymentResponse = payTaxWithTimeOverProcess.execute(PaymentRequest.mock());

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }
}