package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class PaymentCompletedBuilderTest {

    @Test
    void shouldReturnPayment() {

        final String protocolo = Debit.mock().getProtocol();

        final PaymentCompleted paymentCompleted = PaymentCompletedBuilder
                .builder()
                .withPayments(Set.of(Payment.mock()))
                .withProtocolo(protocolo)
                .build();

        Assertions
                .assertNotNull(paymentCompleted);

        Assertions
                .assertEquals(protocolo, paymentCompleted.protocolo());

        Assertions
                .assertFalse(paymentCompleted.payments().isEmpty());

        Assertions
                .assertEquals(1, paymentCompleted.payments().size());

        final Payment payment = paymentCompleted.payments().iterator().next();

        Assertions
                .assertEquals(DocumentoArrecadacao.mock().getCodigoDeBarra(), payment.codigoDeBarra());

        Assertions
                .assertEquals(Debit.mock().getProtocol(), payment.numeroAutenticacao());

    }
}