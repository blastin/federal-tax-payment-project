package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class UseCaseConsultPaymentTest {

    @Test
    void shouldReturnResponse() {

        final UseCaseConsultPayment useCaseConsultPayment = UseCaseConsultPayment.mock();

        AssertPayment.completed(useCaseConsultPayment.consult(Debit.mock().getProtocol()));

    }

    @Test
    void shouldReturnProtocolInvalid() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                UseCaseConsultPaymentFactory.factory(statusPayment, null, null);

        final String protocol = "protocol";

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        AssertPayment
                .error(
                        MessageError.INVALIDO,
                        FieldError.PROTOCOL,
                        statusPayment.invalidRequest(),
                        paymentResponse,
                        protocol
                );

    }

    @Test
    void shouldReturnResponseComplete() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                UseCaseConsultPaymentFactory.factory(statusPayment, DebitGateway.mock(), TaxRevenuesQuery.mock());

        final String protocol = Debit.mock().getProtocol();

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        AssertPayment
                .completed(paymentResponse);

    }

    @Test
    void shouldReturnNotFoundProtocol() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                UseCaseConsultPaymentFactory.factory(statusPayment, new DebitGateway.SkeletalDebitGateway() {

                    @Override
                    public Optional<Debit> recoveryByProtocol(String protocol) {
                        return Optional.empty();
                    }
                }, null);

        final String protocol = Debit.mock().getProtocol();

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        AssertPayment
                .error
                        (
                                MessageError.INEXISTENTE,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                protocol
                        );

    }

}