package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.DebitQuery;
import org.junit.jupiter.api.Test;

class PaymentDuplicationProcessTest {

    @Test
    void shouldReturnDebitExistingByProtocol() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process processDuplicate =
                new PaymentDuplicationProcess(null, statusPayment, new DebitQuery.SkeletalDebitQuery() {
                    @Override
                    public boolean existsDebitByProtocol(String protocol) {
                        return true;
                    }
                });

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = processDuplicate.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.PAGAMENTO_DUPLICADO,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                paymentRequest.obterProtocolo()
                        );

    }

    @Test
    void shouldReturnDebitNotExistingByProtocol() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process processDuplicate =
                new PaymentDuplicationProcess(null, statusPayment, DebitQuery.mock());

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = processDuplicate.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

}