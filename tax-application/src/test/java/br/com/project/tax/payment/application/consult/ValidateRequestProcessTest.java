package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Test;

class ValidateRequestProcessTest {

    @Test
    void shouldReturnResponseNullBecauseProtocolIsValid() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment = new ValidateRequestProcess(null, statusPayment);

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(Debit.mock().getProtocol());

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldReturnResponseErrorBecauseProtocolIsInvalid() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment = new ValidateRequestProcess(null, statusPayment);

        final String protocol = "";

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        AssertPayment.error
                (
                        MessageError.INVALIDO,
                        FieldError.PROTOCOL,
                        statusPayment.invalidRequest(),
                        paymentResponse,
                        protocol
                );

    }

}