package br.com.project.tax.payment.application.response;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PaymentErrorBuilderTest {

    @Test
    void shouldBuildPaymentError() {

        final MessageError codigoDeBarraLimite = MessageError.CODIGO_DE_BARRA_LIMITE;

        final FieldError codigoDeBarra = FieldError.CODIGO_DE_BARRA;

        final DocumentoArrecadacao documentoArrecadacao = DocumentoArrecadacao.mock();

        final PaymentError paymentError =
                PaymentErrorBuilder
                        .builder()
                        .field(codigoDeBarra)
                        .withMessageError(codigoDeBarraLimite)
                        .value(documentoArrecadacao.getCodigoDeBarra())
                        .build();

        Assertions
                .assertEquals(codigoDeBarraLimite.toString(), paymentError.description());

        Assertions
                .assertEquals(codigoDeBarraLimite.toInt(), paymentError.code());

        Assertions
                .assertEquals(codigoDeBarra.toString(), paymentError.field());

        Assertions
                .assertEquals(documentoArrecadacao.getCodigoDeBarra(), paymentError.value());

    }

}