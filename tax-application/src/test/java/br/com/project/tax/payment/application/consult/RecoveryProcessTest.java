package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class RecoveryProcessTest {

    @Test
    void shouldReturnResponseNullBecauseDebitNotFound() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                new RecoveryProcess(null, statusPayment, new DebitQuery.SkeletalDebitQuery() {
                    @Override
                    public Optional<Debit> recoveryByProtocol(String protocol) {
                        return Optional.empty();
                    }
                }, null);

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(Debit.mock().getProtocol());

        Assertions.assertNotNull(paymentResponse);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldReturnResponseErrorBecauseTaxRevenuesNotRecovery() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                new RecoveryProcess(null, statusPayment, DebitQuery.mock(), protocol -> Optional.empty());

        final Debit debit = Debit.mock();

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(debit.getProtocol());

        Assertions.assertNotNull(paymentResponse);

        AssertPayment
                .error
                        (
                                MessageError.PAGAMENTO_NAO_EFETUADO,
                                FieldError.SITUACAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                debit.getDebitSituation().toString()
                        );

    }

    @Test
    void shouldReturnResponseComplete() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment =
                new RecoveryProcess(null, statusPayment, DebitQuery.mock(), TaxRevenuesQuery.mock());

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(Debit.mock().getProtocol());

        Assertions.assertNotNull(paymentResponse);

        AssertPayment.completed(paymentResponse);

    }

}