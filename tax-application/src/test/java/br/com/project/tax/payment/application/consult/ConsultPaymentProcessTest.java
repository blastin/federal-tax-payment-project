package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class ConsultPaymentProcessTest {

    @Test
    void shouldReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment = new ConsultPaymentProcess(null, statusPayment) {
            @Override
            protected Optional<PaymentResponse> process(final String protocol) {
                return Optional.empty();
            }
        };

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(Debit.mock().getProtocol());

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

    @Test
    void shouldReturnResponseComplete() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment = new ConsultPaymentProcess(null, statusPayment) {
            @Override
            protected Optional<PaymentResponse> process(final String protocol) {
                return Optional.of(PaymentResponse.mock());
            }
        };

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(Debit.mock().getProtocol());

        AssertPayment.completed(paymentResponse);

    }

}