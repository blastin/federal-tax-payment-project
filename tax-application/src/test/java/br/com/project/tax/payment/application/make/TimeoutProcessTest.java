package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.TemporizeBuilder;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

class TimeoutProcessTest {

    @Test
    void shouldReturnErrorResponseOfPaymentProcess() {

        final LocalDateTime now = LocalDateTime.now();

        final String dataRequisicao = Datas.formatarData(now.toLocalDate());

        final String horaRequisicao = Datas.formatarHora(now.minusMinutes(1).toLocalTime());

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterDataRequisicao() {
                return dataRequisicao;
            }

            @Override
            public String obterHoraRequisicao() {
                return horaRequisicao;
            }

        };

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process = new TimeoutProcess(statusPayment, null, TemporizeBuilder.builder().toPayment(0).build());

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        final String dataFormatada = Datas.paraDataHora(dataRequisicao, horaRequisicao).toString();

        AssertPayment.error
                (
                        MessageError.TEMPO_EXCEDIDO,
                        FieldError.HORA_REQUISICAO,
                        statusPayment.invalidRequest(),
                        paymentResponse, dataFormatada
                );

    }

    @Test
    void shouldReturnErrorResponseOfNullObject() {

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process = new TimeoutProcess(statusPayment, null, TemporizeBuilder.builder().initIs(10).build());

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

    }

}