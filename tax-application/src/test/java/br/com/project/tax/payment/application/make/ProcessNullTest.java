package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import org.junit.jupiter.api.Test;

class ProcessNullTest {

    @Test
    void deveProcessarComNulo() {

        final Process process = Process.mock();

        final PaymentResponse paymentResponse = process.execute(null);

        AssertPayment.errorNull(StatusPayment.mock(), paymentResponse);

    }

}