package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitGateway;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransactionException;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.TemporizeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

class UseCaseMakePaymentTest {

    private static String localDateTimeToString(final PaymentRequest paymentRequest) {
        return Datas
                .paraDataHora(paymentRequest.obterDataRequisicao(), paymentRequest.obterHoraRequisicao())
                .toString();
    }

    @Test
    void mock() {

        final UseCaseMakePayment mock = UseCaseMakePayment.mock();

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = mock.execute(paymentRequest);

        Assertions.assertNotNull(paymentRequest);

        AssertPayment.completed(paymentResponse, paymentRequest);

    }

    @Test
    void shouldBeInvalidRequest() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        null,
                                        null,
                                        null,
                                        null
                                );

        final PaymentRequest paymentRequest = new PaymentRequest.SkeletalPaymentRequestMock() {

            @Override
            public String obterProtocolo() {
                return "super.obterProtocolo();";
            }

        };

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        Assertions.assertNotNull(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.INVALIDO,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                paymentRequest.obterProtocolo()
                        );

    }

    @Test
    void shouldBeTimeout() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        null,
                                        null,
                                        null,
                                        Temporize.mock()
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        Assertions.assertNotNull(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.TEMPO_EXCEDIDO,
                                FieldError.HORA_REQUISICAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                localDateTimeToString(paymentRequest)
                        );

    }

    @Test
    void shouldBePaymentDuplicated() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        new DebitGateway.SkeletalDebitGateway() {
                                            @Override
                                            public boolean existsDebitByProtocol(String protocol) {
                                                return true;
                                            }
                                        },
                                        null,
                                        null,
                                        TemporizeBuilder.FIVE
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        Assertions.assertNotNull(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.PAGAMENTO_DUPLICADO,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                paymentRequest.obterProtocolo()
                        );

    }

    @Test
    void shouldBeNotEnableCustomer() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        DebitGateway.mock(),
                                        customer -> false,
                                        null,
                                        TemporizeBuilder.FIVE
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        Assertions.assertNotNull(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.CLIENTE_NAO_HABILITADO,
                                FieldError.CONTRIBUINTE,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                paymentRequest.obterContribuinte().getNumero()
                        );

    }

    @Test
    void shouldBeReversalPayment() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicInteger chamadas = new AtomicInteger();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        new DebitGateway.SkeletalDebitGateway() {
                                            @Override
                                            public void save(final Debit debit) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                                chamadas.incrementAndGet();
                                            }
                                        },
                                        customer -> true,
                                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                            @Override
                                            public void reversal(final String autenticationNumber) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public void save(TaxRevenues taxRevenues) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                                chamadas.incrementAndGet();
                                                return Optional.of(TaxRevenues.mock());
                                            }
                                        },
                                        TemporizeBuilder.builder().initIs(10).toPayment(0).build()
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.TEMPO_EXCEDIDO,
                                FieldError.HORA_REQUISICAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                localDateTimeToString(paymentRequest)
                        );

        Assertions.assertEquals(4, chamadas.get());

    }

    @Test
    void shouldBeFeePaid() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicInteger chamadas = new AtomicInteger();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        new DebitGateway.SkeletalDebitGateway() {
                                            @Override
                                            public void save(final Debit debit) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                                chamadas.incrementAndGet();
                                            }
                                        },
                                        customer -> true,
                                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                            @Override
                                            public void reversal(final String autenticationNumber) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public void save(TaxRevenues taxRevenues) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) throws TaxRevenuesTransactionException {
                                                chamadas.incrementAndGet();
                                                return super.payFederalFee(taxRevenuesPayment);
                                            }
                                        },
                                        TemporizeBuilder.FIVE
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        AssertPayment
                .completed
                        (
                                paymentResponse,
                                paymentRequest
                        );

        Assertions.assertEquals(4, chamadas.get());

    }

    @Test
    void shouldRejectPayment() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicInteger chamadas = new AtomicInteger();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        new DebitGateway.SkeletalDebitGateway() {
                                            @Override
                                            public void save(final Debit debit) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                                chamadas.incrementAndGet();
                                            }
                                        },
                                        customer -> true,
                                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                            @Override
                                            public void reversal(final String autenticationNumber) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public void save(TaxRevenues taxRevenues) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
                                                chamadas.incrementAndGet();
                                                return Optional.empty();
                                            }
                                        },
                                        TemporizeBuilder.FIVE
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.REJEITADO,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                paymentRequest.obterProtocolo()
                        );


        Assertions.assertEquals(3, chamadas.get());

    }

    @Test
    void shouldBePaymentError() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final AtomicInteger chamadas = new AtomicInteger();

        final UseCaseMakePayment useCaseMakePayment =
                UseCaseMakePaymentFactory
                        .factory
                                (
                                        statusPayment,
                                        new DebitGateway.SkeletalDebitGateway() {
                                            @Override
                                            public void save(final Debit debit) {
                                                chamadas.incrementAndGet();
                                            }

                                            @Override
                                            public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                                chamadas.incrementAndGet();
                                            }
                                        },
                                        customer -> true,
                                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                                            @Override
                                            public void reversal(final String autenticationNumber) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public void save(TaxRevenues taxRevenues) {
                                                throw new RuntimeException();
                                            }

                                            @Override
                                            public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) throws TaxRevenuesTransactionException {
                                                chamadas.incrementAndGet();
                                                throw new TaxRevenuesTransactionException("Error transaction");
                                            }
                                        },
                                        TemporizeBuilder.FIVE
                                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.TEMPO_EXCEDIDO,
                                FieldError.HORA_REQUISICAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                localDateTimeToString(paymentRequest)
                        );


        Assertions.assertEquals(3, chamadas.get());

    }

}
