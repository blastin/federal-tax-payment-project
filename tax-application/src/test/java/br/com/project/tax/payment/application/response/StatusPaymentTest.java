package br.com.project.tax.payment.application.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StatusPaymentTest {

    @Test
    void statusPaymentShouldReturnIntegers() {

        final StatusPayment statusPayment = StatusPayment.mock();

        Assertions
                .assertEquals(0, statusPayment.processRequest());

        Assertions
                .assertEquals(1, statusPayment.invalidRequest());

        Assertions
                .assertEquals(2, statusPayment.errorProcess());

    }

}