package br.com.project.tax.payment.application.consult;

import br.com.project.tax.payment.application.make.AssertPayment;
import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.Debit;
import org.junit.jupiter.api.Test;

class DebitNotFoundProcessTest {

    @Test
    void shouldReturnResponseNotFoundProtocol() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final UseCaseConsultPayment useCaseConsultPayment = new DebitNotFoundProcess(null, statusPayment);

        final String protocol = Debit.mock().getProtocol();

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        AssertPayment
                .error
                        (
                                MessageError.INEXISTENTE,
                                FieldError.PROTOCOL,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                protocol
                        );

    }
}