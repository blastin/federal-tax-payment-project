package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.FieldError;
import br.com.project.tax.payment.application.response.MessageError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransactionException;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

class PaymentErrorProcessTest {

    @Test
    void shouldBePaymentErrorAndReturnResponseTimeout() {

        final AtomicBoolean debitSituationAtomicBoolean = new AtomicBoolean(false);

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process = new PaymentErrorProcess
                (
                        null,
                        statusPayment,
                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                            @Override
                            public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment)
                                    throws TaxRevenuesTransactionException {
                                throw new TaxRevenuesTransactionException("Error transaction");
                            }
                        },
                        null,
                        new DebitCommand.SkeletalDebitCommand() {
                            @Override
                            public void updateDebitSituation(String protocol, DebitSituation debitSituation) {
                                debitSituationAtomicBoolean.set(debitSituation.equals(DebitSituation.FALHA_PROCESSAMENTO));
                            }
                        },
                        Temporize.mock()
                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment
                .error
                        (
                                MessageError.TEMPO_EXCEDIDO,
                                FieldError.HORA_REQUISICAO,
                                statusPayment.invalidRequest(),
                                paymentResponse,
                                Datas.paraDataHora(paymentRequest.obterDataRequisicao(), paymentRequest.obterHoraRequisicao()).toString()
                        );

    }

    @Test
    void shouldBeNotPaidAndReturnResponseNull() {

        final StatusPayment statusPayment = StatusPayment.mock();

        final Process process = new PaymentErrorProcess
                (
                        null,
                        statusPayment,
                        new TaxRevenuesGateway.SkeletalTaxRevenuesGateway() {
                            @Override
                            public Optional<TaxRevenues> payFederalFee(TaxRevenuesPayment taxRevenuesPayment) {
                                return Optional.empty();
                            }
                        },
                        null,
                        new DebitCommand.SkeletalDebitCommand() {
                            @Override
                            public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
                                throw new RuntimeException();
                            }
                        },
                        Temporize.mock()
                );

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = process.execute(paymentRequest);

        AssertPayment
                .errorNull
                        (
                                statusPayment,
                                paymentResponse
                        );

    }
}