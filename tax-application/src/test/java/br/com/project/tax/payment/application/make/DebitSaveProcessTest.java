package br.com.project.tax.payment.application.make;

import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class DebitSaveProcessTest {

    @Test
    void shouldSaveDebit() {

        final AtomicReference<Debit> debitAtomicReference = new AtomicReference<>();

        final StatusPayment statusPayment = StatusPayment.mock();

        final DebitSaveProcess makeThePaymentDebitSaveProcess =
                new DebitSaveProcess(null, statusPayment, new DebitCommand() {
                    @Override
                    public void save(Debit debit) {
                        debitAtomicReference.set(debit);
                    }

                    @Override
                    public void updateDebitSituation(String protocol, DebitSituation debitSituation) {
                        throw new RuntimeException("Shouldn't get here!");
                    }
                });

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final PaymentResponse paymentResponse = makeThePaymentDebitSaveProcess.execute(paymentRequest);

        AssertPayment.errorNull(statusPayment, paymentResponse);

        final Debit debit = debitAtomicReference.get();

        Assertions
                .assertNotNull(debit);

        Assertions
                .assertEquals(DebitSituation.PENDENTE, debit.getDebitSituation());

        Assertions
                .assertEquals(paymentRequest.obterProtocolo(), debit.getProtocol());

        Assertions
                .assertEquals(paymentRequest.obterEspecieDebito(), debit.getEspecieDebito().toInt());

        Assertions
                .assertEquals(paymentRequest.obterDespachante(), debit.getDespachante());

        Assertions
                .assertEquals(paymentRequest.obterDataRequisicao(), debit.getDataRequisicao());

        Assertions
                .assertEquals(paymentRequest.obterHoraRequisicao(), debit.getHoraRequisicao());

        Assertions
                .assertEquals(paymentRequest.obterReferenciaDebito(), debit.getReferenciaDebito());

        Assertions
                .assertEquals(
                        paymentRequest.obterCodigosDeBarra(),
                        debit.getArrecadacoes()
                                .stream()
                                .map(DocumentoArrecadacao::getCodigoDeBarra)
                                .collect(Collectors.toUnmodifiableList()));

    }

}