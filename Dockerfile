FROM lisboajeff/openjdk11-jre:alpine

COPY tax-adapters/tax-spring-web/target/*.jar application.jar

CMD ["java", "-jar", "-Dspring.profiles.active=${ENVIRONMENT_PROFILE}", "application.jar"]
