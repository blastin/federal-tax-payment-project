package br.com.project.tax.payment.web.config;

import br.com.project.tax.payment.domain.customer.CustomerQuery;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenuesCommand;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;
import br.com.project.tax.payment.web.adapter.customer.CustomerQueryAdapter;
import br.com.project.tax.payment.web.adapter.customer.CustomerRepository;
import br.com.project.tax.payment.web.adapter.debit.DebitCommandAdapter;
import br.com.project.tax.payment.web.adapter.debit.DebitQueryAdapter;
import br.com.project.tax.payment.web.adapter.debit.DebitRepository;
import br.com.project.tax.payment.web.adapter.debit.SituacaoDebitoRepository;
import br.com.project.tax.payment.web.adapter.tax.TaxRevenuesCommandAdapter;
import br.com.project.tax.payment.web.adapter.tax.TaxRevenuesQueryAdapter;
import br.com.project.tax.payment.web.adapter.tax.TaxRevenuesRepository;
import br.com.project.tax.payment.web.adapter.tax.TaxRevenuesTransactionAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class PersistenceConfiguration {

    @Bean
    DebitQuery debitQuery(final DebitRepository debitRepository) {
        return new DebitQueryAdapter(debitRepository);
    }

    @Bean
    DebitCommand debitCommand
            (
                    final DebitRepository debitRepository,
                    final SituacaoDebitoRepository situacaoDebitoRepository
            ) {
        return new DebitCommandAdapter(debitRepository, situacaoDebitoRepository);
    }

    @Bean
    CustomerQuery costumerQuery(final CustomerRepository customerRepository) {
        return new CustomerQueryAdapter(customerRepository);
    }

    @Bean
    TaxRevenuesCommand taxRevenuesCommand
            (
                    final TaxRevenuesRepository taxRevenuesRepository,
                    final DebitRepository debitRepository
            ) {
        return new TaxRevenuesCommandAdapter(taxRevenuesRepository, debitRepository);
    }

    @Bean
    TaxRevenuesTransaction taxRevenuesTransaction() {
        return new TaxRevenuesTransactionAdapter();
    }

    @Bean
    TaxRevenuesQuery taxRevenuesQuery(final TaxRevenuesRepository taxRevenuesRepository) {
        return new TaxRevenuesQueryAdapter(taxRevenuesRepository);
    }

}
