package br.com.project.tax.payment.web.config;

import br.com.project.tax.payment.application.consult.UseCaseConsultPayment;
import br.com.project.tax.payment.application.consult.UseCaseConsultPaymentFactory;
import br.com.project.tax.payment.application.make.UseCaseMakePayment;
import br.com.project.tax.payment.application.make.UseCaseMakePaymentFactory;
import br.com.project.tax.payment.application.response.StatusPayment;
import br.com.project.tax.payment.domain.customer.CustomerQuery;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitGateway;
import br.com.project.tax.payment.domain.debito.DebitQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenuesCommand;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;
import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.web.facade.DebitGatewayFacade;
import br.com.project.tax.payment.web.facade.TaxRevenuesGatewayFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
public class UseCasesConfiguration {

    @Bean
    TaxRevenuesGateway taxRevenuesGateway
            (
                    final TaxRevenuesCommand taxRevenuesCommand,
                    final TaxRevenuesTransaction taxRevenuesTransaction,
                    final TaxRevenuesQuery taxRevenuesQuery
            ) {
        return new TaxRevenuesGatewayFacade(taxRevenuesCommand, taxRevenuesTransaction, taxRevenuesQuery);
    }

    @Bean
    DebitGateway debitGateway(final DebitCommand debitCommand, final DebitQuery debitQuery) {
        return new DebitGatewayFacade(debitCommand, debitQuery);
    }

    @Bean
    StatusPayment statusPayment() {

        return new StatusPayment() {
            @Override
            public int processRequest() {
                return HttpStatus.CREATED.value();
            }

            @Override
            public int invalidRequest() {
                return HttpStatus.UNPROCESSABLE_ENTITY.value();
            }

            @Override
            public int errorProcess() {
                return HttpStatus.EXPECTATION_FAILED.value();
            }
        };

    }

    @Bean
    UseCaseMakePayment makeThePaymentUseCase
            (
                    final StatusPayment statusPayment,
                    final DebitGateway debitGateway,
                    final CustomerQuery customerQuery,
                    final TaxRevenuesGateway taxRevenuesGateway,
                    final Temporize temporize
            ) {
        return UseCaseMakePaymentFactory
                .factory
                        (
                                statusPayment,
                                debitGateway,
                                customerQuery,
                                taxRevenuesGateway,
                                temporize
                        );
    }

    @Bean
    UseCaseConsultPayment useCaseConsultPayment
            (
                    final StatusPayment statusPayment,
                    final DebitGateway debitGateway,
                    final TaxRevenuesGateway taxRevenuesGateway
            ) {
        return UseCaseConsultPaymentFactory.factory(statusPayment, debitGateway, taxRevenuesGateway);

    }

}
