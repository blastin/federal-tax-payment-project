package br.com.project.tax.payment.web.model;

import br.com.project.tax.payment.application.response.Payment;
import br.com.project.tax.payment.application.response.PaymentCompleted;
import br.com.project.tax.payment.application.response.PaymentError;
import br.com.project.tax.payment.application.response.PaymentResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class PaymentResponseModel {

    private static class PaymentCompletedModel {

        private static class PaymentModel {

            private static Set<PaymentModel> of(final Set<Payment> payments) {
                return payments.stream().map(PaymentModel::new).collect(Collectors.toUnmodifiableSet());
            }

            private PaymentModel(final Payment payment) {
                this.payment = payment;
            }

            private final Payment payment;

            public String getCodigoDeBarra() {
                return payment.codigoDeBarra();
            }

            public String getNumeroAutenticacao() {
                return payment.numeroAutenticacao();
            }

            public String getDataArrecadacao() {
                return payment.dataArrecadacao();
            }

            public String getHoraArrecadacao() {
                return payment.horaArrecadacao();
            }

        }

        private static PaymentCompletedModel of(final PaymentCompleted paymentCompleted) {
            return new PaymentCompletedModel(paymentCompleted);
        }

        private PaymentCompletedModel(final PaymentCompleted paymentCompleted) {
            this.paymentCompleted = paymentCompleted;
        }

        private final PaymentCompleted paymentCompleted;

        public String getProtocol() {
            return paymentCompleted.protocolo();
        }

        public Set<PaymentModel> getPayments() {
            return PaymentModel.of(paymentCompleted.payments());
        }

    }

    private static class PaymentErrorModel {

        private static Set<PaymentErrorModel> of(final Set<PaymentError> errors) {

            return
                    errors
                            .stream()
                            .map(PaymentErrorModel::new)
                            .collect(Collectors.toUnmodifiableSet());

        }

        private PaymentErrorModel(final PaymentError paymentError) {
            this.paymentError = paymentError;
        }

        private final PaymentError paymentError;

        public int getCode() {
            return paymentError.code();
        }

        public String getDescription() {
            return paymentError.description();
        }

        public String getField() {
            return paymentError.field();
        }

        public String getValue() {
            return paymentError.value();
        }

    }

    public static PaymentResponseModel of(final PaymentResponse paymentResponse) {
        return new PaymentResponseModel(paymentResponse);
    }

    private PaymentResponseModel(final PaymentResponse paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    private final PaymentResponse paymentResponse;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Set<PaymentErrorModel> getErrors() {
        return PaymentErrorModel
                .of(paymentResponse.errors());
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public PaymentCompletedModel getData() {
        return paymentResponse
                .payment()
                .map(PaymentCompletedModel::of)
                .orElse(null);
    }

}
