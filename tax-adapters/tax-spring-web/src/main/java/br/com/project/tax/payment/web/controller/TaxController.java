package br.com.project.tax.payment.web.controller;

import br.com.project.tax.payment.application.consult.UseCaseConsultPayment;
import br.com.project.tax.payment.application.make.UseCaseMakePayment;
import br.com.project.tax.payment.application.response.PaymentResponse;
import br.com.project.tax.payment.web.model.PaymentRequestModel;
import br.com.project.tax.payment.web.model.PaymentResponseModel;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/federal-tax-payment")
public class TaxController {

    TaxController(final UseCaseMakePayment useCaseMakePayment, final UseCaseConsultPayment useCaseConsultPayment) {
        this.useCaseMakePayment = useCaseMakePayment;
        this.useCaseConsultPayment = useCaseConsultPayment;
    }

    private final UseCaseMakePayment useCaseMakePayment;

    private final UseCaseConsultPayment useCaseConsultPayment;

    @GetMapping(path = "{protocol}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponseModel> consultTaxPayment(@PathVariable final String protocol) {

        final PaymentResponse paymentResponse = useCaseConsultPayment.consult(protocol);

        final PaymentResponseModel paymentResponseModel = PaymentResponseModel.of(paymentResponse);

        return ResponseEntity.status(paymentResponse.status()).body(paymentResponseModel);

    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponseModel> makeTaxPayment(@RequestBody final PaymentRequestModel paymentRequest) {

        final PaymentResponse paymentResponse = useCaseMakePayment.execute(paymentRequest);

        final PaymentResponseModel paymentResponseModel = PaymentResponseModel.of(paymentResponse);

        return ResponseEntity.status(paymentResponse.status()).body(paymentResponseModel);

    }

}
