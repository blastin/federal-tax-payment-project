package br.com.project.tax.payment.web.adapter.debit;

import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.debito.DebitSituation;

public final class DebitCommandAdapter implements DebitCommand {

    public DebitCommandAdapter
            (
                    final DebitRepository debitRepository,
                    final SituacaoDebitoRepository situacaoDebitoRepository
            ) {
        this.debitRepository = debitRepository;
        this.situacaoDebitoRepository = situacaoDebitoRepository;
    }

    private final DebitRepository debitRepository;

    private final SituacaoDebitoRepository situacaoDebitoRepository;

    @Override
    public void save(final Debit debit) {
        debitRepository.save(new DebitEntity(debit));
    }

    @Override
    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {

        final SituacaoDebitoEntity situacaoDebitoEntity = debitRepository
                .findByProtocol(protocol)
                .map(debitModel -> new SituacaoDebitoEntity(debitSituation, debitModel))
                .orElseThrow(() -> new RuntimeException("não encontrei debito com protocolo : " + protocol));

        situacaoDebitoRepository.save(situacaoDebitoEntity);

    }

}
