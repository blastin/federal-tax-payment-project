package br.com.project.tax.payment.web.adapter.debit;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "documento_arrecadacao")
public class DocumentoArrecadacaoModel {

    public DocumentoArrecadacaoModel() {

    }

    DocumentoArrecadacaoModel(final DocumentoArrecadacao documentoArrecadacao, final DebitEntity debitEntity) {
        codigoDeBarra = documentoArrecadacao.getCodigoDeBarra();
        numeroDocumento = documentoArrecadacao.getNumeroDocumento();
        valor = documentoArrecadacao.getValor();
        this.debitEntity = debitEntity;
    }

    @GeneratedValue
    @Id
    private UUID id;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    private String codigoDeBarra;

    public String getCodigoDeBarra() {
        return codigoDeBarra;
    }

    public void setCodigoDeBarra(String codigoDeBarra) {
        this.codigoDeBarra = codigoDeBarra;
    }

    private String numeroDocumento;

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    private BigDecimal valor;

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "debit_id")
    private DebitEntity debitEntity;

    public DebitEntity getDebitModel() {
        return debitEntity;
    }

    public void setDebitModel(final DebitEntity debitEntity) {
        this.debitEntity = debitEntity;
    }

}
