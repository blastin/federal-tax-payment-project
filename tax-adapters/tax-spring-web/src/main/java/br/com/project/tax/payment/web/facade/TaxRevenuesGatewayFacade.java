package br.com.project.tax.payment.web.facade;

import br.com.project.tax.payment.domain.tax.*;

import java.util.Optional;

public final class TaxRevenuesGatewayFacade implements TaxRevenuesGateway {

    public TaxRevenuesGatewayFacade
            (
                    final TaxRevenuesCommand taxRevenuesCommand,
                    final TaxRevenuesTransaction taxRevenuesTransaction,
                    final TaxRevenuesQuery taxRevenuesQuery
            ) {
        this.taxRevenuesCommand = taxRevenuesCommand;
        this.taxRevenuesTransaction = taxRevenuesTransaction;
        this.taxRevenuesQuery = taxRevenuesQuery;
    }

    private final TaxRevenuesCommand taxRevenuesCommand;

    private final TaxRevenuesTransaction taxRevenuesTransaction;

    private final TaxRevenuesQuery taxRevenuesQuery;

    @Override
    public void save(final TaxRevenues taxRevenues) {
        taxRevenuesCommand.save(taxRevenues);
    }

    @Override
    public void reversal(final String autenticationNumber) {
        taxRevenuesTransaction.reversal(autenticationNumber);
    }

    @Override
    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment)
            throws TaxRevenuesTransactionException {
        return taxRevenuesTransaction.payFederalFee(taxRevenuesPayment);
    }

    @Override
    public Optional<TaxRevenues> recoveryByProtocol(final String protocol) {
        return taxRevenuesQuery.recoveryByProtocol(protocol);
    }

}
