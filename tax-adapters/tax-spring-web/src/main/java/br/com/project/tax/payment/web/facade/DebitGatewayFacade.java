package br.com.project.tax.payment.web.facade;

import br.com.project.tax.payment.domain.debito.*;

import java.util.Optional;

public final class DebitGatewayFacade implements DebitGateway {

    public DebitGatewayFacade(final DebitCommand debitCommand, final DebitQuery debitQuery) {
        this.debitCommand = debitCommand;
        this.debitQuery = debitQuery;
    }

    private final DebitCommand debitCommand;

    private final DebitQuery debitQuery;

    @Override
    public void save(final Debit debit) {
        debitCommand.save(debit);
    }

    @Override
    public void updateDebitSituation(final String protocol, final DebitSituation debitSituation) {
        debitCommand.updateDebitSituation(protocol, debitSituation);
    }

    @Override
    public boolean existsDebitByProtocol(final String protocol) {
        return debitQuery.existsDebitByProtocol(protocol);
    }

    @Override
    public Optional<Debit> recoveryByProtocol(final String protocol) {
        return debitQuery.recoveryByProtocol(protocol);
    }

}
