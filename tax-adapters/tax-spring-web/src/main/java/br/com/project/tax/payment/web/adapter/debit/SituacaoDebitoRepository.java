package br.com.project.tax.payment.web.adapter.debit;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SituacaoDebitoRepository extends JpaRepository<SituacaoDebitoEntity, Long> {
}
