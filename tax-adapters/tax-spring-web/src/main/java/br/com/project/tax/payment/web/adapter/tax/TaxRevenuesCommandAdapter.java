package br.com.project.tax.payment.web.adapter.tax;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesCommand;
import br.com.project.tax.payment.web.adapter.debit.DebitRepository;

public final class TaxRevenuesCommandAdapter implements TaxRevenuesCommand {

    public TaxRevenuesCommandAdapter
            (
                    final TaxRevenuesRepository taxRevenuesRepository,
                    final DebitRepository debitRepository
            ) {
        this.taxRevenuesRepository = taxRevenuesRepository;
        this.debitRepository = debitRepository;
    }

    private final TaxRevenuesRepository taxRevenuesRepository;

    private final DebitRepository debitRepository;

    @Override
    public void save(final TaxRevenues taxRevenues) {

        final TaxRevenuesEntity taxRevenuesEntity = debitRepository
                .findByProtocol(taxRevenues.getProtocol())
                .map(debitModel -> new TaxRevenuesEntity(taxRevenues, debitModel))
                .orElseThrow(() -> new RuntimeException("não encontrei debit por protocolo :" + taxRevenues.getProtocol()));

        taxRevenuesRepository.save(taxRevenuesEntity);

    }

}
