package br.com.project.tax.payment.web.adapter.customer;

import br.com.project.tax.payment.domain.customer.Customer;
import br.com.project.tax.payment.domain.customer.CustomerQuery;
import org.springframework.data.domain.Example;

public final class CustomerQueryAdapter implements CustomerQuery {

    public CustomerQueryAdapter(final CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    private final CustomerRepository customerRepository;

    @Override
    public boolean existsCostumer(final Customer customer) {
        return customerRepository.exists(Example.of(new CustomerEntity(customer)));
    }

}
