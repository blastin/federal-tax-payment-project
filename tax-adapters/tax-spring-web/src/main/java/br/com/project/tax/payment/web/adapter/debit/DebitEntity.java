package br.com.project.tax.payment.web.adapter.debit;

import br.com.project.tax.payment.domain.darf.DocumentoArrecadacao;
import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitSituation;
import br.com.project.tax.payment.domain.debito.EspecieDebito;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@Entity
@Table(name = "debito")
public class DebitEntity {

    static Debit toDebit(final DebitEntity debitEntity) {
        return
                Debit
                        .builder()
                        .paraEspecie(debitEntity.especieDebito)
                        .paraHora(debitEntity.horaRequisicao)
                        .paraData(debitEntity.dataRequisicao)
                        .comProtocolo(debitEntity.protocol)
                        .feitaPeloDespachante(debitEntity.despachante)
                        .comArrecadacoes
                                (
                                        debitEntity
                                                .documentosArrecadacaoModel
                                                .stream()
                                                .map(DocumentoArrecadacaoModel::getCodigoDeBarra)
                                                .map(DocumentoArrecadacao::de)
                                                .collect(Collectors.toUnmodifiableSet())
                                )
                        .situacaoDebito
                                (
                                        debitEntity
                                                .situacaoDebitoEntities
                                                .stream()
                                                .sorted(Comparator.reverseOrder())
                                                .max(SituacaoDebitoEntity::compareTo)
                                                .map(SituacaoDebitoEntity::toDebitSituation)
                                                .orElse(DebitSituation.NAO_ENCONTRADO)
                                )
                        .build();
    }

    public DebitEntity() {
    }

    DebitEntity(final Debit debit) {
        protocol = debit.getProtocol();
        dataRequisicao = debit.getDataRequisicao();
        horaRequisicao = debit.getHoraRequisicao();
        despachante = debit.getDespachante();
        especieDebito = debit.getEspecieDebito();
        documentosArrecadacaoModel =
                debit.getArrecadacoes()
                        .stream()
                        .map
                                (
                                        documentoArrecadacao ->
                                                new DocumentoArrecadacaoModel(documentoArrecadacao, this)
                                )
                        .collect(Collectors.toUnmodifiableSet());
        situacaoDebitoEntities = Set.of(new SituacaoDebitoEntity(debit.getDebitSituation(), this));
    }

    @GeneratedValue
    @Id
    private UUID id;

    public void setId(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    private String protocol;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(final String protocol) {
        this.protocol = protocol;
    }

    private String dataRequisicao;

    public String getDataRequisicao() {
        return dataRequisicao;
    }

    public void setDataRequisicao(final String dataRequisicao) {
        this.dataRequisicao = dataRequisicao;
    }

    private String horaRequisicao;

    public String getHoraRequisicao() {
        return horaRequisicao;
    }

    public void setHoraRequisicao(final String horaRequisicao) {
        this.horaRequisicao = horaRequisicao;
    }

    private String despachante;

    public String getDespachante() {
        return despachante;
    }

    public void setDespachante(final String despachante) {
        this.despachante = despachante;
    }

    private EspecieDebito especieDebito;

    public EspecieDebito getEspecieDebito() {
        return especieDebito;
    }

    public void setEspecieDebito(final EspecieDebito especieDebito) {
        this.especieDebito = especieDebito;
    }

    @OneToMany(mappedBy = "debitEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<DocumentoArrecadacaoModel> documentosArrecadacaoModel;

    public Set<DocumentoArrecadacaoModel> getDocumentosArrecadacaoModel() {
        return documentosArrecadacaoModel;
    }

    public void setDocumentosArrecadacaoModel(final Set<DocumentoArrecadacaoModel> documentosArrecadacaoModel) {
        this.documentosArrecadacaoModel = documentosArrecadacaoModel;
    }

    @OneToMany(mappedBy = "debitEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<SituacaoDebitoEntity> situacaoDebitoEntities;

    public Set<SituacaoDebitoEntity> getSituacaoDebitoModels() {
        return situacaoDebitoEntities;
    }

    public void setSituacaoDebitoModels(final Set<SituacaoDebitoEntity> situacaoDebitoEntities) {
        this.situacaoDebitoEntities = situacaoDebitoEntities;
    }

}
