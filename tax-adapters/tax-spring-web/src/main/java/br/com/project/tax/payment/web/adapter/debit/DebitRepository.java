package br.com.project.tax.payment.web.adapter.debit;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DebitRepository extends JpaRepository<DebitEntity, Long> {

    boolean existsByProtocol(final String protocol);

    Optional<DebitEntity> findByProtocol(final String protocol);

}
