package br.com.project.tax.payment.web.model;

import br.com.project.tax.payment.application.make.PaymentRequest;
import br.com.project.tax.payment.domain.contribuinte.Contribuinte;

import java.util.List;

@SuppressWarnings("unused")
public final class PaymentRequestModel implements PaymentRequest {

    public static PaymentRequestModel of(final PaymentRequest paymentRequest) {
        return new PaymentRequestModel(paymentRequest);
    }

    public PaymentRequestModel() {
    }

    private PaymentRequestModel(final PaymentRequest paymentRequest) {
        protocolo = paymentRequest.obterProtocolo();
        banco = paymentRequest.obterBanco();
        agencia = paymentRequest.obterAgencia();
        contaCorrente = paymentRequest.obterContaCorrente();
        despachante = paymentRequest.obterDespachante();
        contribuinte = paymentRequest.obterContribuinte();
        especieDebito = paymentRequest.obterEspecieDebito();
        referenciaDebito = paymentRequest.obterReferenciaDebito();
        dataRequisicao = paymentRequest.obterDataRequisicao();
        horaRequisicao = paymentRequest.obterHoraRequisicao();
        codigos = paymentRequest.obterCodigosDeBarra();
    }

    private String protocolo;

    @Override
    public String obterProtocolo() {
        return protocolo;
    }

    public void setProtocolo(final String protocolo) {
        this.protocolo = protocolo;
    }

    private String banco;

    @Override
    public String obterBanco() {
        return banco;
    }

    public void setBanco(final String banco) {
        this.banco = banco;
    }

    private String agencia;

    @Override
    public String obterAgencia() {
        return agencia;
    }

    public void setAgencia(final String agencia) {
        this.agencia = agencia;
    }

    private String contaCorrente;

    @Override
    public String obterContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(final String contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    private String despachante;

    @Override
    public String obterDespachante() {
        return despachante;
    }

    public void setDespachante(final String despachante) {
        this.despachante = despachante;
    }

    private Contribuinte contribuinte;

    @Override
    public Contribuinte obterContribuinte() {
        return contribuinte;
    }

    public ContribuinteModel getContribuinte() {
        return new ContribuinteModel(contribuinte);
    }

    private static class ContribuinteModel {

        public ContribuinteModel() {
        }

        private ContribuinteModel(final Contribuinte contribuinte) {
            valor = contribuinte.getNumero();
            tipo = contribuinte.getCodigo();
        }

        private String valor;

        private int tipo;

        public void setValor(String valor) {
            this.valor = valor;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

    }

    public void setContribuinte(final ContribuinteModel contribuinte) {
        this.contribuinte = Contribuinte
                .builder()
                .comTipo(contribuinte.tipo)
                .paraNumero(contribuinte.valor)
                .build();
    }

    private int especieDebito;

    @Override
    public int obterEspecieDebito() {
        return especieDebito;
    }

    public void setEspecieDebito(final int especieDebito) {
        this.especieDebito = especieDebito;
    }

    private String referenciaDebito;

    @Override
    public String obterReferenciaDebito() {
        return referenciaDebito;
    }

    public void setReferenciaDebito(final String referenciaDebito) {
        this.referenciaDebito = referenciaDebito;
    }

    private String dataRequisicao;

    @Override
    public String obterDataRequisicao() {
        return dataRequisicao;
    }

    public void setDataRequisicao(final String dataRequisicao) {
        this.dataRequisicao = dataRequisicao;
    }

    private String horaRequisicao;

    @Override
    public String obterHoraRequisicao() {
        return horaRequisicao;
    }

    public void setHoraRequisicao(final String horaRequisicao) {
        this.horaRequisicao = horaRequisicao;
    }

    private List<String> codigos;

    @Override
    public List<String> obterCodigosDeBarra() {
        return codigos;
    }

    public void setCodigos(final List<String> codigos) {
        this.codigos = codigos;
    }

}
