package br.com.project.tax.payment.web.adapter.debit;

import br.com.project.tax.payment.domain.debito.DebitSituation;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "situacao_debito")
public class SituacaoDebitoEntity implements Comparable<SituacaoDebitoEntity> {

    static DebitSituation toDebitSituation(final SituacaoDebitoEntity situacaoDebitoEntity) {
        return DebitSituation.porCodigo(situacaoDebitoEntity.codigo);
    }

    public SituacaoDebitoEntity() {
    }

    SituacaoDebitoEntity(final DebitSituation debitSituation, final DebitEntity debitEntity) {
        codigo = debitSituation.toInt();
        registro = LocalDateTime.now();
        this.debitEntity = debitEntity;
    }

    @Id
    @GeneratedValue
    private UUID id;

    public void setId(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    private int codigo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(final int codigo) {
        this.codigo = codigo;
    }

    private LocalDateTime registro;

    public LocalDateTime getRegistro() {
        return registro;
    }

    public void setRegistro(final LocalDateTime registro) {
        this.registro = registro;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "debit_id")
    private DebitEntity debitEntity;

    public DebitEntity getDebitModel() {
        return debitEntity;
    }

    public void setDebitModel(final DebitEntity debitEntity) {
        this.debitEntity = debitEntity;
    }

    @Override
    public int compareTo(final SituacaoDebitoEntity situacaoDebitoEntity) {
        return registro.compareTo(situacaoDebitoEntity.registro);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SituacaoDebitoEntity that = (SituacaoDebitoEntity) o;
        return getCodigo() == that.getCodigo() &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getRegistro(), that.getRegistro()) &&
                Objects.equals(debitEntity, that.debitEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCodigo(), getRegistro(), debitEntity);
    }

}
