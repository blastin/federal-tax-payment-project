package br.com.project.tax.payment.web.config;

import br.com.project.tax.payment.domain.time.Temporize;
import br.com.project.tax.payment.domain.time.TemporizeBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AttributesConfiguration {

    @Bean
    public Temporize temporize(@Value("${temporize.init:0}") final int init, @Value("${temporize.payment:0}") final int payment) {
        return TemporizeBuilder.builder().initIs(init).toPayment(payment).build();
    }

}
