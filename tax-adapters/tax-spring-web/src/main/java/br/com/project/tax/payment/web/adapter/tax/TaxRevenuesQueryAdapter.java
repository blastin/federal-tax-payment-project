package br.com.project.tax.payment.web.adapter.tax;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesQuery;

import java.util.Optional;

public final class TaxRevenuesQueryAdapter implements TaxRevenuesQuery {

    public TaxRevenuesQueryAdapter(final TaxRevenuesRepository taxRevenuesRepository) {
        this.taxRevenuesRepository = taxRevenuesRepository;
    }

    private final TaxRevenuesRepository taxRevenuesRepository;

    @Override
    public Optional<TaxRevenues> recoveryByProtocol(final String protocol) {
        return taxRevenuesRepository.findByDebitEntity_Protocol(protocol).map(TaxRevenuesEntity::toTaxRevenues);
    }

}
