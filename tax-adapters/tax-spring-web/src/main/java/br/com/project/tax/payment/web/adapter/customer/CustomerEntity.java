package br.com.project.tax.payment.web.adapter.customer;

import br.com.project.tax.payment.domain.customer.Customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "customer")
public class CustomerEntity {

    public CustomerEntity() {
    }

    CustomerEntity(final Customer customer) {
        contaCorrente = customer.getContaCorrente();
        codigoAgencia = customer.getCodigoAgencia();
        codigoBanco = customer.getCodigoBanco();
    }

    @GeneratedValue
    @Id
    private UUID id;

    public void setId(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    private String contaCorrente;

    public String getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(final String contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    private String codigoAgencia;

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(final String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    private String codigoBanco;

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(final String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

}
