package br.com.project.tax.payment.web.adapter.tax;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.web.adapter.debit.DebitEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

@SuppressWarnings("unused")
@Entity
@Table(name = "tax_revenues")
public class TaxRevenuesEntity {

    static TaxRevenues toTaxRevenues(final TaxRevenuesEntity taxRevenuesEntity) {

        return

                TaxRevenues
                        .builder()
                        .protocolIs(taxRevenuesEntity.debitEntity.getProtocol())
                        .taxRevenuesPaymentInDate(taxRevenuesEntity.taxRevenuesDate.toString(), DateTimeFormatter.ISO_DATE)
                        .taxRevenuesPaymentInTime(taxRevenuesEntity.taxRevenuesTime.toString(), DateTimeFormatter.ISO_TIME)
                        .withNumber(taxRevenuesEntity.autenticationNumber)
                        .build();

    }

    public TaxRevenuesEntity() {
    }

    TaxRevenuesEntity(final TaxRevenues taxRevenues, final DebitEntity debitEntity) {
        autenticationNumber = taxRevenues.getAutenticationNumber();
        taxRevenuesTime = taxRevenues.getTaxRevenuesTime();
        taxRevenuesDate = taxRevenues.getTaxRevenuesDate();
        this.debitEntity = debitEntity;
    }

    @Id
    @GeneratedValue
    private UUID id;

    public void setId(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    private String autenticationNumber;

    public String getAutenticationNumber() {
        return autenticationNumber;
    }

    public void setAutenticationNumber(String autenticationNumber) {
        this.autenticationNumber = autenticationNumber;
    }

    private LocalTime taxRevenuesTime;

    public LocalTime getTaxRevenuesTime() {
        return taxRevenuesTime;
    }

    public void setTaxRevenuesTime(LocalTime taxRevenuesTime) {
        this.taxRevenuesTime = taxRevenuesTime;
    }

    private LocalDate taxRevenuesDate;

    public LocalDate getTaxRevenuesDate() {
        return taxRevenuesDate;
    }

    public void setTaxRevenuesDate(LocalDate taxRevenuesDate) {
        this.taxRevenuesDate = taxRevenuesDate;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "debit_id")
    private DebitEntity debitEntity;

    public DebitEntity getDebitModel() {
        return debitEntity;
    }

    public void setDebitModel(final DebitEntity debitEntity) {
        this.debitEntity = debitEntity;
    }

}
