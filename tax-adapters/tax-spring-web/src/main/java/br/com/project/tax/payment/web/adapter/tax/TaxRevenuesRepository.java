package br.com.project.tax.payment.web.adapter.tax;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaxRevenuesRepository extends JpaRepository<TaxRevenuesEntity, Long> {

    Optional<TaxRevenuesEntity> findByDebitEntity_Protocol(final String protocol);

}
