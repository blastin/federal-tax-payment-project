package br.com.project.tax.payment.web.adapter.debit;

import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitQuery;

import java.util.Optional;

public final class DebitQueryAdapter implements DebitQuery {

    public DebitQueryAdapter(final DebitRepository debitRepository) {
        this.debitRepository = debitRepository;
    }

    private final DebitRepository debitRepository;

    @Override
    public boolean existsDebitByProtocol(final String protocol) {
        return debitRepository.existsByProtocol(protocol);
    }

    @Override
    public Optional<Debit> recoveryByProtocol(final String protocol) {
        return debitRepository.findByProtocol(protocol).map(DebitEntity::toDebit);
    }

}
