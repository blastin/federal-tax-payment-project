package br.com.project.tax.payment.web.adapter.tax;

import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import br.com.project.tax.payment.domain.tax.TaxRevenuesTransaction;

import java.util.Optional;

public final class TaxRevenuesTransactionAdapter implements TaxRevenuesTransaction {

    @Override
    public Optional<TaxRevenues> payFederalFee(final TaxRevenuesPayment taxRevenuesPayment) {
        return Optional.of(TaxRevenues.mock());
    }

    @Override
    public void reversal(final String autenticationNumber) {
        // reversal
    }

}
