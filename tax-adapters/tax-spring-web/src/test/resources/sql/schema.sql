create table if not exists customer
(
    id             binary(16)   not null
        primary key,
    codigo_agencia varchar(255) not null,
    codigo_banco   varchar(255) not null,
    conta_corrente varchar(255) not null
);

create table if not exists debito
(
    id              binary(16)   not null
        primary key,
    data_requisicao varchar(255) not null,
    despachante     varchar(255) not null,
    especie_debito  int          not null,
    hora_requisicao varchar(255) not null,
    protocol        varchar(255) not null
);

create table if not exists documento_arrecadacao
(
    id               binary(16)     not null
        primary key,
    codigo_de_barra  varchar(255)   not null,
    numero_documento varchar(255)   not null,
    valor            decimal(19, 2) not null,
    debit_id         binary(16)     not null,
    foreign key (debit_id) references debito (id)
);

create table if not exists situacao_debito
(
    id       binary(16)  not null
        primary key,
    codigo   int         not null,
    registro datetime(6) not null,
    debit_id binary(16)  not null,
    foreign key (debit_id) references debito (id)
);

create table if not exists tax_revenues
(
    id                   binary(16)   not null
        primary key,
    autentication_number varchar(255) not null,
    tax_revenues_date    date         not null,
    tax_revenues_time    time         not null,
    debit_id             binary(16)   not null,
    foreign key (debit_id) references debito (id)
);

