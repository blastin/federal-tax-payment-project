package br.com.project.tax.payment.web;

import br.com.project.tax.payment.application.make.PaymentRequest;
import br.com.project.tax.payment.domain.time.Datas;
import br.com.project.tax.payment.domain.time.Temporize;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class TimeOutRequestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private @MockBean
    Temporize temporize;

    @Test
    void shouldReturnTimeExceeded() throws Exception {

        final PaymentRequest paymentRequest = PaymentRequest.mock();

        final String json = RequestMock.toJson(paymentRequest);

        Mockito
                .when(temporize.init())
                .thenReturn(0L);

        mockMvc
                .perform(post("/federal-tax-payment")
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json(String.format("{\n" +
                                        "    \"errors\": [\n" +
                                        "        {\n" +
                                        "            \"code\": 8,\n" +
                                        "            \"description\": \"Tempo máximo excedido\",\n" +
                                        "            \"field\": \"horaRequisicao\",\n" +
                                        "            \"value\": \"%s\"\n" +
                                        "        }\n" +
                                        "    ]\n" +
                                        "}", Datas.paraDataHora
                                        (
                                                paymentRequest.obterDataRequisicao(),
                                                paymentRequest.obterHoraRequisicao()).toString()
                                )
                        )
                );

        Mockito.verify(temporize, Mockito.atLeastOnce()).init();

    }

}
