package br.com.project.tax.payment.web;

import br.com.project.tax.payment.domain.debito.Debit;
import br.com.project.tax.payment.domain.debito.DebitCommand;
import br.com.project.tax.payment.domain.tax.TaxRevenues;
import br.com.project.tax.payment.domain.tax.TaxRevenuesCommand;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class GetTaxControllerITest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnProtocolInvalid() throws Exception {

        final String protocol = "shouldReturnProtocolInvalid";

        mockMvc
                .perform(get("/federal-tax-payment/{protocol}", protocol))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json("{\n" +
                                "    \"errors\": [\n" +
                                "        {\n" +
                                "            \"code\": 1,\n" +
                                "            \"description\": \"Campo invalido\",\n" +
                                "            \"field\": \"protocolo\",\n" +
                                "            \"value\": \"shouldReturnProtocolInvalid\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"));


    }

    @Autowired
    private DebitCommand debitCommand;

    @Autowired
    private TaxRevenuesCommand taxRevenuesCommand;

    @Test
    void shouldReturnDebit() throws Exception {

        final Debit debit = Debit.mock();

        final String protocol = debit.getProtocol();

        debitCommand.save(Debit.mock());

        taxRevenuesCommand.save(TaxRevenues.mock());

        mockMvc
                .perform(get("/federal-tax-payment/{protocol}", protocol))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.protocol").value(protocol))
                .andExpect(jsonPath("$.data.payments[0].codigoDeBarra")
                        .value(debit.getArrecadacoes().iterator().next().getCodigoDeBarra()))
                .andExpect(jsonPath("$.data.payments[0].numeroAutenticacao").value(protocol));

    }


    @Test
    void shouldReturnNotFoundProtocol() throws Exception {

        final Debit debit = Debit.mock();

        final String protocol = debit.getProtocol().replace('0', '1');

        mockMvc
                .perform(get("/federal-tax-payment/{protocol}", protocol))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json(String.format("{\n" +
                                "    \"errors\": [\n" +
                                "        {\n" +
                                "            \"code\": 10,\n" +
                                "            \"description\": \"Número de prótocolo inexistente\",\n" +
                                "            \"field\": \"protocolo\",\n" +
                                "            \"value\": \"%s\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}", protocol)));

    }

}
