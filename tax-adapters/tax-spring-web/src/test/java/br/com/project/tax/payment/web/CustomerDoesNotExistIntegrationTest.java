package br.com.project.tax.payment.web;

import br.com.project.tax.payment.application.make.PaymentRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class CustomerDoesNotExistIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnCostumerDoesNotExist() throws Exception {

        final String json = RequestMock.toJson(new PaymentRequest.SkeletalPaymentRequestMock() {
            @Override
            public String obterAgencia() {
                return "0124";
            }

            @Override
            public String obterProtocolo() {
                return "512345678901234560";
            }
        });

        mockMvc
                .perform(post("/federal-tax-payment")
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json("{\n" +
                                "    \"errors\": [\n" +
                                "        {\n" +
                                "            \"code\": 9,\n" +
                                "            \"description\": \"Pagador não está habilitado a pagar taxa de atributo federal\",\n" +
                                "            \"field\": \"Contribuinte\",\n" +
                                "            \"value\": \"219.786.850-09\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"));

    }

}
