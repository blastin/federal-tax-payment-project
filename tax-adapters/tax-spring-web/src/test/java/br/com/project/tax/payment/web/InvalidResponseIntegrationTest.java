package br.com.project.tax.payment.web;

import br.com.project.tax.payment.application.make.PaymentRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class InvalidResponseIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnInvalidResponse() throws Exception {

        final String json = RequestMock.toJson(new PaymentRequest.SkeletalPaymentRequestMock() {
            @Override
            public String obterDataRequisicao() {
                return "super.obterDataRequisicao()";
            }
        });

        mockMvc
                .perform(post("/federal-tax-payment")
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json("{\n" +
                                "    \"errors\": [\n" +
                                "        {\n" +
                                "            \"code\": 1,\n" +
                                "            \"description\": \"Campo invalido\",\n" +
                                "            \"field\": \"dataRequisicao\",\n" +
                                "            \"value\": \"super.obterDataRequisicao()\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"));

    }

}
