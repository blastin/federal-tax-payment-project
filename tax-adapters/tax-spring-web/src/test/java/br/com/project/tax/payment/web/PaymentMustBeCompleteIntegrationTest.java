package br.com.project.tax.payment.web;

import br.com.project.tax.payment.application.make.PaymentRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class PaymentMustBeCompleteIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldResponseComplete() throws Exception {

        final String json = RequestMock.toJson(new PaymentRequest.SkeletalPaymentRequestMock() {
            @Override
            public String obterProtocolo() {
                return "992345678901234567";
            }
        });

        mockMvc
                .perform(post("/federal-tax-payment")
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.protocol")
                        .value("992345678901234567"))
                .andExpect(jsonPath("$.data.payments[0].codigoDeBarra")
                        .value("85870000004734504321701907161701904720320212"))
                .andExpect(jsonPath("$.data.payments[0].numeroAutenticacao")
                        .value("012345678901234567"));

    }
}
