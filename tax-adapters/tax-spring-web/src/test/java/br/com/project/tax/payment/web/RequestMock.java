package br.com.project.tax.payment.web;

import br.com.project.tax.payment.application.make.PaymentRequest;
import br.com.project.tax.payment.web.model.PaymentRequestModel;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class RequestMock {

    static String toJson(final PaymentRequest paymentRequest) throws JsonProcessingException {
        return toJson(PaymentRequestModel.of(paymentRequest));
    }

    static String toJson(final PaymentRequestModel paymentRequestModel) throws JsonProcessingException {

        final ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        return objectMapper.writeValueAsString(paymentRequestModel);

    }

    private RequestMock() {

    }

}
