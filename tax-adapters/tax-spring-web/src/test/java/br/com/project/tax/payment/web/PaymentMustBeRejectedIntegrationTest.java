package br.com.project.tax.payment.web;


import br.com.project.tax.payment.application.make.PaymentRequest;
import br.com.project.tax.payment.domain.tax.TaxRevenuesGateway;
import br.com.project.tax.payment.domain.tax.TaxRevenuesPayment;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class PaymentMustBeRejectedIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaxRevenuesGateway taxRevenuesGateway;

    @Test
    void shouldReturnResponseRejectedPayment() throws Exception {

        final String json = RequestMock.toJson(new PaymentRequest.SkeletalPaymentRequestMock() {
            @Override
            public String obterProtocolo() {
                return "012341111111234561";
            }
        });

        Mockito
                .when(taxRevenuesGateway.payFederalFee(Mockito.any(TaxRevenuesPayment.class)))
                .thenReturn(Optional.empty());

        mockMvc
                .perform(post("/federal-tax-payment")
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content()
                        .json("{\n" +
                                "    \"errors\": [\n" +
                                "        {\n" +
                                "            \"code\": 3,\n" +
                                "            \"description\": \"rejeitado\",\n" +
                                "            \"field\": \"protocolo\",\n" +
                                "            \"value\": \"012341111111234561\"\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"));


        Mockito
                .verify(taxRevenuesGateway, Mockito.times(1))
                .payFederalFee(Mockito.any(TaxRevenuesPayment.class));

    }

}
